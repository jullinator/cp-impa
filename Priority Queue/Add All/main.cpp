/*
*
*/
#include <iostream>
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#define INF 2000000000

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main (){
    ios::sync_with_stdio(false);
    
    while(true){
        int n;
        cin >> n;
        if(!n) break;
        priority_queue <int, vi, greater<int>> pq;
        for(int n0 = 0; n0 < n; n0++){
            int x;
            cin >> x;
            pq.push(x);
        }
        int cost = 0;
        
        while(pq.size() >= 2){
            int y = pq.top(); pq.pop();
            int x = pq.top(); pq.pop();
            //cout << y << " " << x << endl;
            int res = y+x;
            cost += res;
            if(pq.empty()) break;
            pq.push(res);
        }
        cout << cost << endl;

    }
    return 0;
}