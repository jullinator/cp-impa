from collections import defaultdict
n = int(input())
unique = defaultdict(int)
dice = [0] * n
for i in range(n):
    temp = [int(x) for x in input().split()]
    h = frozenset((frozenset((temp[2*j], temp[2*j+1])) for j in range(3)))
    unique[h] += 1
    dice[i] = h
print(len(unique))