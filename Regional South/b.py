import math
eps = 1e-4 # remember: <=

class Key(object):
    def __init__(self, name, iter):
        self.notes = set(iter)
        self.name = name

notes = [
    tuple("A"),
    ("A#", "Bb"),
    tuple("B"),
    tuple("C"),
    ("C#", "Db"),
    tuple("D"),
    ("D#", "Eb"),
    tuple("E"),
    tuple("F"),
    ("F#", "Gb"),
    tuple("G"),
    ("G#", "Ab")
]

possible = set(range(5))
"""
keys = [
    set([10, 0, 2, 3, 5, 7, 9]),
    set([3,5,7,8,10,0,2]),
    set([6,8,10,11,1,3,5]),
    set([9,11,0,2,4,5,7]),
    set([10,0,1,3,5,6,8])
]

key_names = [
    "G major",
    "C major",
    "E"
]
"""

keys = [
    Key("G major", ["G", "A", "B", "C", "D", "E", "F#"]),
    Key("C major", ["C", "D", "E", "F", "G", "A", "B"]),
    Key("Eb Major", ["Eb", "F", "G", "Ab", "Bb", "C", "D"]),
    Key("F# minor", ["F#", "G#", "A", "B", "C#", "D", "E"]),
    Key("G minor", ["G", "A", "Bb", "C", "D", "Eb", "F"])
]

n = int(raw_input())
res = [list() for _ in xrange(5)]
for _ in xrange(n):
    f = float(input())
    a = (12*( math.log(f) - math.log(440) ))/math.log(2)
    idx = int(round(a)%12)
    #print idx, notes[idx]
    for i in possible.copy():
        for note in notes[idx]:
            if note in keys[i].notes:
                res[i].append(note)
                break
        else:
            possible.remove(i)
if len(possible) == 1:
    idx = possible.pop()
    print keys[idx].name
    for el in res[idx]:
        print el
else:
    print "cannot determine key"