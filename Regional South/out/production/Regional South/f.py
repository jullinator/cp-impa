from collections import defaultdict
T = int(input())

for t0 in range(T):
    n = int(input())
    items = defaultdict(int)
    for _ in range(n):
        name, num = input().split()
        num = int(num)
        items[name] += num 
    res = [(-num, name) for name, num in items.items()]
    print(len(res))
    for num, name in sorted(res):
        num = -num 
        print(name, num)
        