n, q = map(int, input().split())
locations = [0] + [int(x) for x in input().split()]

for _ in range(q):
    cmd, x, y = map(int, input().split())
    if cmd == 1:
        locations[x] = y 
    else:
        print(abs(locations[x]-locations[y]))
        

