T = int(input())

for t0 in range(T):
    n = int(input())
    res = 1
    while n>1:
        res *= n 
        n -= 1
    print(res%10)