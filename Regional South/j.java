import java.util.*;
import java.io.*;

public class j {

    private static class Edge {
        int capacity = 1, flow = 0, to, from;
        Edge backEdge;

	public Edge(final int from, final int to) {
	    this.to = to;
	    this.from = from;
	}
    }

    public static int augment(List<List<Edge>> adj){
	Queue<Integer> q = new LinkedList<>();
	Edge pred[] = new Edge[adj.size()];
	q.add(0);
	while(!q.isEmpty()){
	    int v = q.poll();
	    //System.out.println(v-2);
	    if (v == 1){
	        break;
	    }
	    for(Edge e: adj.get(v)){
		//System.out.print(" " + e.to);
	        if(e.to != 0 && pred[e.to] == null && e.capacity-e.flow > 0){
		    q.add(e.to);
		    pred[e.to] = e;
		}

	    }
	}

	Edge e = pred[1];
	if(e == null) return -1;
	int count = 0;
	while(e != null){
	    //System.out.print(e.to+" ");
	    if(e.flow < 0) count -= 2;
	    e.flow += 1;
	    e.backEdge.flow -= 1;
	    e = pred[e.from];
	    count += 1;
	}
        return count;
    }

    public static void main(String[] args) throws Exception {
	try(Kattio io = new Kattio(System.in, System.out)){
	    int P = io.getInt(), R = io.getInt(), L = io.getInt();
	    List<List<Edge>> adj = new ArrayList<>();
	    for (int i = 0; i < R+2; i++) {
		adj.add(new ArrayList<>());
	    }
	    for (int i = 0; i < L; i++) {
		int v1 = io.getInt()+2, v2 = io.getInt()+2;
		Edge e1 = new Edge(v1, v2);
		Edge e2 = new Edge(v2, v1);
		e1.backEdge = e2;
		e2.backEdge = e1;
		adj.get(v1).add(e1);
		adj.get(v2).add(e2);
	    }
	    int flow = 0;
	    int time = 0;
	    int t = augment(adj);
	    while(t != -1 && flow < P){
	        flow ++;
	        time += t;
	        t = augment(adj);
	    }
	    if (flow >= P){
		io.println(time);

	    } else {
	        io.printf("%d people left behind\n", P-flow);
	    }
	}
    }


    private static class Kattio extends PrintWriter {
        public Kattio(InputStream i) {
            super(new BufferedOutputStream(System.out));
            r = new BufferedReader(new InputStreamReader(i));
        }
        public Kattio(InputStream i, OutputStream o) {
            super(new BufferedOutputStream(o));
            r = new BufferedReader(new InputStreamReader(i));
        }

        public boolean hasMoreTokens() {
            return peekToken() != null;
        }

        public int getInt() {
            return Integer.parseInt(nextToken());
        }

        public double getDouble() {
            return Double.parseDouble(nextToken());
        }

        public long getLong() {
            return Long.parseLong(nextToken());
        }

        public String getWord() {
            return nextToken();
        }



        private BufferedReader r;
        private String line;
        private StringTokenizer st;
        private String token;

        private String peekToken() {
            if (token == null)
                try {
                    while (st == null || !st.hasMoreTokens()) {
                        line = r.readLine();
                        if (line == null) return null;
                        st = new StringTokenizer(line);
                    }
                    token = st.nextToken();
                } catch (IOException e) { }
            return token;
        }

        private String nextToken() {
            String ans = peekToken();
            token = null;
            return ans;
        }
    }
}
