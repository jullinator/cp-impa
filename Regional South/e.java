import java.lang.reflect.Array;
import java.util.*;
import java.io.*;

public class e {

    private static class Disc {
        int r, x, y;

	public Disc(final int x, final int y, final int r) {
	    this.r = r;
	    this.x = x;
	    this.y = y;
	}

	public double distTo(final Disc o){
	    return Math.hypot(x-o.x, y-o.y) - r - o.r;
	}
    }

    private static class Item implements Comparable<Item> {
        int id;
        double weight;

	public Item(final int id, final double weight) {
	    this.id = id;
	    this.weight = weight;
	}

	@Override public int compareTo(final Item o) {
	    return weight == o.weight ? Integer.compare(id, o.id ): Double.compare(weight, o.weight);
	}
    }

    public static void main(String[] args) {
	try(Kattio io = new Kattio(System.in, System.out)){
	    int n = io.getInt();
	    List<Disc> discs = new ArrayList<>();
	    for (int i = 0; i < n; i++) {
		discs.add(new Disc(io.getInt(), io.getInt(), io.getInt()));
	    }
	    PriorityQueue<Item> pq = new PriorityQueue<>();
	    double [] dist = new double[n];
	    Arrays.fill(dist, Double.MAX_VALUE);
	    double totalWeight = 0.0;
	    HashSet<Integer> unvisited = new HashSet<>();
	    for (int i = 1; i < n; i++) {
		unvisited.add(i);
	    }
	    pq.add(new Item(0, 0));
	    while(!pq.isEmpty()){
	        Item item = pq.poll();
	        if(item.weight > dist[item.id]) continue;
	        unvisited.remove(item.id);
	        totalWeight += item.weight;
	        for(int v2: unvisited){
	            double weight = discs.get(item.id).distTo(discs.get(v2));
	            if (weight < dist[v2]){
	                dist[v2] = weight;
	                pq.add(new Item(v2, weight));
		    }
		}
	    }
	    System.out.println(totalWeight);
	}

    }

    private static class Kattio extends PrintWriter {
        public Kattio(InputStream i) {
            super(new BufferedOutputStream(System.out));
            r = new BufferedReader(new InputStreamReader(i));
        }
        public Kattio(InputStream i, OutputStream o) {
            super(new BufferedOutputStream(o));
            r = new BufferedReader(new InputStreamReader(i));
        }

        public boolean hasMoreTokens() {
            return peekToken() != null;
        }

        public int getInt() {
            return Integer.parseInt(nextToken());
        }

        public double getDouble() {
            return Double.parseDouble(nextToken());
        }

        public long getLong() {
            return Long.parseLong(nextToken());
        }

        public String getWord() {
            return nextToken();
        }



        private BufferedReader r;
        private String line;
        private StringTokenizer st;
        private String token;

        private String peekToken() {
            if (token == null)
                try {
                    while (st == null || !st.hasMoreTokens()) {
                        line = r.readLine();
                        if (line == null) return null;
                        st = new StringTokenizer(line);
                    }
                    token = st.nextToken();
                } catch (IOException e) { }
            return token;
        }

        private String nextToken() {
            String ans = peekToken();
            token = null;
            return ans;
        }
    }
}
