/*
* 
*
*
*/
#include <iostream>
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <functional>

#define INF  99999999
using namespace std;

typedef pair<int, int> ii;
typedef tuple<int, int, int, int> i4;
typedef vector<ii> vii;
typedef vector<i4> vi4;



int main (){
    ios::sync_with_stdio(false);
    int tc = 0;
    while(true){

        
        
        int n, m, q, s;
        cin >> n >> m >> q >> s; 
        if (!(n||m||q||s)) break;
        if(tc != 0) cout << endl; tc++;

        vector<vi4> adj(n);

        for(int m1 = 0; m1 < m; m1++){
            int v1, v2,t0, p, w;
            cin >> v1 >> v2 >> t0 >> p >> w; 
            adj[v1].push_back(i4(v2, t0, p, w));
        }

        priority_queue<ii, vii, greater<ii>> pq;

        pq.push(ii(0, s));
        vector<int> dist(n, INF);
        dist[s] = 0;

        while(!pq.empty()){
            ii p1 = pq.top(); pq.pop();
            int t1 = p1.first, v1 = p1.second;
            if (dist[v1] < t1) continue;
            for (i4 p2: adj[v1]){
                int v2 = get<0>(p2), t0 = get<1>(p2), period = get<2>(p2), w2 = get<3>(p2); 
                if(t1 > t0 && period == 0) continue;
                int wait;
                if(t1 <= t0){
                    wait = t0-t1;
                } else {
                    int prev = (t1-t0)%period;
                    wait = (period-wait) % period;
                }
      
                int t2 = wait + t1 + w2;
                if (t2 < dist[v2]){
                    dist[v2] = t2;
                    pq.push(ii(t2, v2));
                }
            }
        }

        for(int q1 = 0; q1 < q ; q1++){
            int t;
            cin >> t; 
            if (dist[t] == INF) cout << "Impossible" << endl;
            else cout << dist[t] << endl;
        }
    }
    return 0;
}