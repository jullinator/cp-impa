#include <bits/stdc++.h>

using namespace std;

int main (){
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    int n, m;
    vector<int> nums;
    vector<int> sums;
    int t = 0;
    while (cin >> n){
        cout << "Case " << ++t << ":\n";
        nums.clear(); nums.resize(n);
        //sums.clear(); sums.resize(n*n);
        for(int i = 0; i<n; i++){
            cin >> nums[i];
        }
        cin >> m;
        for(int m0 = 0; m0<m; m0++){
            int sum;
            cin >> sum;
            int closest = 1999999999;
            int closest_sum = 0;
            for(int i = 0; i < n; i++){
                for(int j=i+1; j<n; j++){
                    if(abs(nums[i]+nums[j]-sum) < closest){
                        closest = abs(nums[i] + nums[j] - sum);
                        closest_sum = nums[i] + nums[j];
                    }
                    
                    //cout << nums[i] + nums[j] << " ";
                }
            }
            cout << "Closest sum to " << sum << " is " << closest_sum << ".\n";
        }
    }
}