#include <cstdio>
#include <cstring>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#define INF 2000000000

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int N, M, s, t, delay, GN;

ii closed [1001][1001];
int weights [1001][1001];
int g_route [1001];


int main (){
    scanf("%d%d\n", &N, &M);
    scanf("%d%d%d%d\n", &s, &t, &delay, &GN); 

    vector<vi> adj (N+1);

    // Read Georges roads
    for(int i = 0; i < GN ; i++){
        scanf("%d", &g_route[i]);
    }

    for(int i = 0; i < M ; i++){
        int v1, v2, w;
        scanf("%d%d%d\n", &v1, &v2, &w);
        weights[v1][v2] = w;
        weights[v2][v1] = w;
        adj[v1].push_back(v2);
        adj[v2].push_back(v1);

    }

    // Set closed roads
    int prev = -1, cur_time = -1*delay;
    for(int i = 0; i < GN ; i++){
        int v = g_route[i]; 
        if (prev != -1){
            int w  = weights[prev][v];
            closed[prev][v] = ii(cur_time, w+cur_time);
            closed[v][prev] = ii(cur_time, w+cur_time);
            cur_time += w;
        } 
        prev = v;
    }

    priority_queue<ii, vii, greater<ii>> pq;
    pq.push(ii(0, s));
    vi dist(N+1, INF);
    dist[s] = 0;

    while(!pq.empty()){
        ii p1 = pq.top(); pq.pop();
        int dist1 = p1.first, v1 = p1.second;
        if (v1 == t) break;
        if(dist1 > dist[v1]) continue;
        for(int v2: adj[v1]){
            int w2 = weights[v1][v2];
            int dist2 = dist1 + w2;
            ii wait = closed[v1][v2];
            int start = wait.first, end = wait.second;
            if(dist1 >= start && dist1 < end){
                dist2 += (end-dist1);
            }
            if (dist2 < dist[v2]){
                dist[v2] = dist2;
                pq.push(ii(dist2, v2));
            }
        }
    }
    printf("%d\n", dist[t]); 

    return 0;
}