#include <bits/stdc++.h>
using namespace std;
typedef pair<int, int> ii;

int main(){
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    int TC, m, L;
    cin >> TC;
    for(int tc = 0; tc < TC; tc++){
        cin >> L >> m;
        queue<int> left, right;
        for(int i = 0; i < m; i++){
            int length;
            string dir;
            cin >> length >> dir;
            if(dir == "left"){
                left.push(length);
            } else {
                right.push(length);
            }
        }
        bool isLeft = true;
        int tours = 0;
        while(!left.empty() or !right.empty()){
            int rem = L*100;
            queue<int> &q = isLeft ? left : right;
            while(!q.empty() && q.front() <= rem){
                rem -= q.front();
                q.pop();
            }
            isLeft = !isLeft;
            tours ++;
        }
        cout << tours << endl;
    }

}