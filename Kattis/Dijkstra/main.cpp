/*
* 
*
*
*/
#include <iostream>
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <functional>

#define INF  99999999
using namespace std;

typedef pair<int, int> ii;
typedef vector<ii> vii;



int main (){
    ios::sync_with_stdio(false);
    int tc = 0;
    while(true){

        
        vii adj [10005];
        int n, m, q, s;
        cin >> n >> m >> q >> s; 
        if (!(n||m||q||s)) break;
        if(tc != 0) cout << endl; tc++;

        for(int m1 = 0; m1 < m; m1++){
            int v1, v2, w;
            cin >> v1 >> v2 >> w; 
            adj[v1].push_back(ii(w, v2));
        }

        priority_queue<ii, vii, greater<ii>> pq;

        pq.push(ii(0, s));
        int dist[10005];
        fill(dist, dist+10005, INF);
        dist[s] = 0;

        while(!pq.empty()){
            ii p1 = pq.top(); pq.pop();
            int d1 = p1.first, v1 = p1.second;
            if (dist[v1] < d1) continue;
            for (ii p2: adj[v1]){
                int w2 = p2.first, v2 = p2.second;
                if (d1 + w2 < dist[v2]){
                    dist[v2] = d1 + w2;
                    pq.push(ii(d1+w2, v2));
                }
            }
        }

        for(int q1 = 0; q1 < q ; q1++){
            int t;
            cin >> t; 
            if (dist[t] == INF) cout << "Impossible" << endl;
            else cout << dist[t] << endl;
        }
    }
    return 0;
}