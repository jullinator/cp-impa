/*
*
*/
#include <cstdio>
#include <cstring>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#define INF 2000000000

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

class Edge {
    public:
        int v, w;
        Edge(int _v, int _w){
            v = _v;
            w = _w;
        }
};

class Node {
    public:
        int dist, v;
        Node (int _dist,  int _v){
            dist = _dist;
            v = _v;
        }
        bool operator () (Node node1, Node node2){
            return node1.dist > node2.dist;
        }

};

int main (){
    int tc = 0;
    while(true){
        int n, m, q, s;
        scanf("%d %d %d &d\n", &n, &m, &q, &s);
        if(n==0) break;
        if(tc != 0) printf("\n");
        tc++;
        vector<vector<Edge>> adj(n);
        for(int i = 0; i < m ; i++){
            int v1, v2, w;
            scanf("%d %d %d\n", &v1, &v2, &w);
            adj[v1].push_back(Edge(v2, w));
        }

        priority_queue <Node, vector<Node>, Node> pq;
        pq.push(Node(0, s));
        vi dist(n, INF);
        dist[s] = 0;

        while(!pq.empty()){
            Node node = pq.top(); pq.pop();
            if(node.dist > dist[node.v]) continue;
            for (Edge edge: adj[node.v]){
                if(edge.w + node.dist < dist[edge.v]){
                    dist[edge.v] = edge.w + node.dist;
                    pq.push(Node(edge.w + node.dist, edge.v));
                }
            }
        }

        for(int i = 0; i < q ; i++){
            int t;
            scanf("%d\n", &t);
            if(dist[t] == INF) printf("Impossible\n");
            else               printf("%d\n", dist[t]);
        }

    }
    return 0;
}