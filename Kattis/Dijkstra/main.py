from heapq import heappush, heappop
from collections import defaultdict
from builtins import input 
from __future__ import print_function

tc = 0
while True:
    n,m,q,s = map(int, input().split())
    if n == 0: 
        break 
    if tc != 0:
        print()
    tc += 1
    A = defaultdict(list)
    for m1 in range(m):
        v1, v2, w = map(int, input().split())
        A[v1].append((v2, w))
    pq = []
    heappush(pq, (0, s))
    dist = [float('inf')]*n 
    dist[s] = 0
    while pq:
        d1, v1 = heappop(pq)
        if d1 < dist[v1]:
            continue 
        for v2, w2 in A[v1]:
            d2 = w2 + d1 
            if d2 < dist[v2]:
                dist[v2] = d2
                heappush(pq, (d2, v2))
    for q1 in range(q):
        t = int(input())
        if dist[t] == float('inf'):
            print("Impossible")
        else:
            print(dist[t])