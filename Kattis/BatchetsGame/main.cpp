#include <bits/stdc++.h>

using namespace std;

int m;
bool dp [1000001];
bool solved [1000001];
int  takes [10];
bool winnable(int x){
    //cout << x << " ";
    if(solved[x]) return dp[x];
    bool win = false;
    for (int i = 0; i < m; i++){
        int y = takes[i];
        if (y>x) continue;
        if(!winnable(x-y)){ // This little change made it AC (at least half the time)
            win = true;
            break;
        }
    }
    solved[x] = true;
    dp[x] = win;
    return dp[x];
}

int main (){
    int stones;
    while(cin >> stones){
        //cout << stones;
        cin >> m;
        //cout << " " << m;
        memset(dp, false, sizeof(dp));
        memset(solved, false, sizeof(solved));
        for (int i = 0; i < m; i++) {
            cin >> takes[i];
            //cout << " " << takes[i];
            dp[takes[i]] = true;
            solved[takes[i]] = true;
        }
        //cout << endl;
        if(winnable(stones)){
            cout << "Stan wins" << endl;
        } else {
            cout << "Ollie wins" << endl;
        }
    }
    return 0;
}