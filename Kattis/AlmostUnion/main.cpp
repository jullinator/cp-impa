#include <bits/stdc++.h>
using namespace std;

class AlmostUnion{
private:
    vector<int> parent;
    vector<int> score;
    vector<int> rank;
    vector<long long> sums;
    int size;
public:
    AlmostUnion(int N){
        size = N;
        parent.resize(2*N);
        sums.resize(N*2);
        for(int i = 0; i < N; i++){
            parent[i] = N+i;
            sums[i] = i;
        }
        for(int i = N; i < 2*N; i++){
            parent[i] = i;
            sums[i] = sums[i-N];
        }
        score.assign(N*2, 1);
        

        rank.assign(N*2, 0);
        
    }
    int find_parent(int v){
        while (parent[v] != v){
            int temp = parent[v];
            parent[v] = parent[parent[v]];
            v = temp;
        }
        return v;
    }
    int get_score(int v){
        int p = find_parent(v);
        return score[p];
    }
    long long get_sum(int v){
        return sums[find_parent(v)];
    }
    void move(int x, int to){
        int p1 = find_parent(x);
        int p2 = find_parent(to);
        if (p1 == p2) return;
        score[p1]--;
        score[p2]++;
        sums[p1] -= x;
        sums[p2] += x;
        parent[x] = p2;
    }
    void merge(int v1, int v2){
        int p1 = find_parent(v1);
        int p2 = find_parent(v2);
        if (p1 == p2) return;
        int& r1 = rank[p1], r2 = rank[p2];
        if (r1 < r2){
            parent[p1] = p2;
            score[p2] += score[p1];
            sums[p2] += sums[p1];
        } else {
            parent[p2] = p1;
            score[p1] += score[p2];
            sums[p1] += sums[p2];
            if (r1 == r2) {
                r1 ++;
            }
        }
    }
};

int main (){
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    int n, m, cmd, v1, v2;
    while(cin >> n >> m){
        AlmostUnion uni(n+1);
        for(int i = 0; i < m; i++){
            cin >> cmd;
            if(cmd == 1){
                cin >> v1 >> v2;
                uni.merge(v1, v2);
            } else if(cmd == 2){
                cin >> v1 >> v2;
                uni.move(v1, v2);
            } else {
                cin >> v1;
                cout << uni.get_score(v1) << " " << uni.get_sum(v1) << endl;
            }
            //if(cmd != 3) cout << uni.get_score(v1) << endl;
        }
    }
    return 0;
}