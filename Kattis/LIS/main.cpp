/*
*
*/
#include <cstdio>
#include <cstring>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#define INF 2000000000
#define MAX_N 100005

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;


int n, numbers[MAX_N];
int pred[MAX_N], best[MAX_N], best_id[MAX_N];
int lis_len, lis_id;

int main (){
    while(scanf("%d\n", &n) != EOF){
        memset(best, -1, sizeof best);
        memset(best_id, -1, sizeof best_id);
        memset(pred, -1, sizeof pred);
        memset(numbers, 0, sizeof numbers);
        lis_id = -1, lis_len = 0;
        for(int i = 0; i < n ; i++){
            int x;
            scanf("%d", &x);
            numbers[i] = x;
            int pos = lower_bound(best, best+lis_len, x) - best;
            best[pos] = x;
            best_id[pos] = i;
            if(pos != 0){
                pred[i] = best_id[pos-1];
            }
            if(pos + 1 > lis_len){
                lis_len = pos + 1;
                lis_id = i;
            }
        }
        printf("%d\n", lis_len);
        vector<int> res(lis_len);
        int next_id = lis_id;
        for(int i = lis_len-1; i >= 0 ; i--){
            res[i] = next_id;
            next_id = pred[next_id];
        }
        for(int i = 0; i < res.size(); i++){
            printf("%d ", res[i]);
        }
        printf("\n");
    }

    return 0;
}