/*
* 
*
*
*/
#include <iostream>
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <string>
#include <vector>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef long long ll;



ll mod_inv (ll a, ll m){
    ll m0 = m, q, r0 = a, r1 = m, x0 = 1, x1 = 0;
    while (r0 != 1){
        ll temp;
        q = r0 / r1;
        temp = r1, r1 = r0 - (q*r1), r0 = temp;
        temp = x1, x1 = x0 - (q*x1), x0 = temp;
    }
    if(x0 < 0) x0 += m0;
    return x0;
}

int main (){
    ios::sync_with_stdio(false);
    int tc;
    cin >> tc;
    while(tc--){
        ll a, n, b, m;
        cin >> a >> n >> b >> m;
        ll K = n*m;
        ll res = 0;
        res += a*m*mod_inv(m,n) % K;
        res += b*n*mod_inv(n,m) % K;
        cout << res%K << " "<< K <<endl;
        
        

    }
    return 0;
}