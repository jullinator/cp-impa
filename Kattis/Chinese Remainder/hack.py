


tc = int(input())

def mod_inv(a, m):
    m0, q = m, 0 
    x0, x1 = 1, 0 
    r0, r1= a, m
    while r0 != 1:
        q = r0/r1 
        r1, r0 = r0 - (q*r1), r1 
        x1, x0 = x0 - (q*x1), x1 
    return x0 + m0 if x0 < 0 else x0 

for tc0 in range(tc):
    a, n, b, m = map(int, input().split())
    K = n*m
    res = a*m*mod_inv(m,n)
    res += b*n*mod_inv(n,m)
    print("%d %d" % (res%K, K))