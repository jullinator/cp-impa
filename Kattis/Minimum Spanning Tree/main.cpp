/*
* 
*
*
*/
#include <iostream>
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <string>
#include <vector>

using namespace std;

typedef vector<int> vi;
typedef tuple<int, int, int> iii;
typedef pair<int, int> ii;
typedef vector<iii> viii;
typedef vector<ii> vii;


int n,m;
viii edges;

class UnionFind {
    private: vi p, rank;
    public:
        UnionFind (int N){
            rank.assign(N, 0);
            p.assign(N, 0);
            for (int i=0; i<N ; i++)
                p[i] = i;
        }
        int find(int x){
            return x == p[x] ? x : p[x] = find(p[x]) ;
        }
        bool isSame(int x, int y){
            return find(x) == find(y);
        }

        void merge(int i, int j){
            if (!isSame(i, j)){
                int x = find(i), y = find(j);
                if (rank[x] < rank[y]) p[x] = y;
                else{
                    p[y] = x;
                    if(rank[x] == rank[y]) rank[x]++;
                }
            }
        }

        
};

int main (){
    ios::sync_with_stdio(false);
    while(true){
        cin >> n >> m;
        if(!(n || m)) break;
        edges.clear();
        for(int m1 = 0; m1 < m ; m1++){
            int v1, v2, w;
            cin >> v1 >> v2 >> w;
            edges.push_back(iii(v1, v2, w));

        }
        sort(edges.begin(), edges.end(), [](iii edge1, iii edge2){
            return get<2>(edge1) < get<2>(edge2);
        });
        vii tree;
        int cost = 0;
        UnionFind sets(n);
        for(iii edge: edges){
            int v1 = get<0>(edge), v2 = get<1>(edge), w = get<2>(edge);
            if(sets.find(v1) == sets.find(v2)) continue;
            sets.merge(v1, v2);
            cost += w;
            if(v1 < v2)  tree.push_back(ii(v1, v2));
            else tree.push_back(ii(v2, v1));
        }

        // check if no min span tree
        int p1 = sets.find(0);
        bool hasTree = true;
        for(int v = 1; v < n; v++){
            if (p1 != sets.find(v)){
                hasTree = false;
                break;
            }
        }

        if(!hasTree){
            cout << "Impossible" << endl;
        } else {
            sort(tree.begin(), tree.end(), [](ii e1, ii e2){
                if(e1.first  == e2.first) return e1.second < e2.second;
                return e1.first < e2.first;

            });
            cout << cost << endl;
            for(ii edge: tree){
                cout << edge.first << " " <<  edge.second << endl;
            }
        }
        
    }
    return 0;
}