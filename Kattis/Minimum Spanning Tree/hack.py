

while True:
    n, m = map(int, input().split())
    if n == 0: break
    edges = [] 

    rank = [0]*n 
    parent = list(range(n))

    def find(v):
        if v == parent[v]: 
            return v
        parent[v] = find(parent[v])
        return parent[v]

    def merge(p1, p2):
        if(rank[p1] < rank[p2]):
            parent[p1] = p2 
        else:
            parent[p2] = p1 
            if rank[p1] == rank[p2]:
                rank[p1] += 1

    for m1 in range(m):
        v1, v2, w = map(int, input().split())
        edges.append((v1, v2, w))
    edges.sort(key = lambda seq: seq[2])

    cost = 0
    tree = [] 
    for v1, v2, w in edges:
        p1, p2 = find(v1), find(v2)
        if p1 == p2:
            continue 
        merge(p1, p2)
        cost += w 
        if v1<v2: tree.append((v1, v2))
        else: tree.append((v2, v1))

    is_tree = True
    prev = find(0)
    for v in range(1, n):
        if find(v) != prev:
            is_tree = False 
            break 
    if not is_tree:
        print("Impossible")
    else:
        print(cost)
        tree.sort(key = lambda seq: (seq[0], seq[1]))
        for v1, v2 in tree:
            print("%d %d"%(v1, v2))
    
    