#include <bits/stdc++.h>

using namespace std;

vector<int> front;
vector<double> ratios;
int f, r;

int main (){
    while(cin >> f){
        if (f == 0) break;
        cin >> r;
        front.resize(f);
        ratios.resize(f*r);
        for(int i = 0; i < f; i++) cin >> front[i];
        for(int i = 0; i < r; i++){
            int n;
            cin >> n;
            for(int j = 0; j < f; j++){
                ratios[i*f + j] = (double) n/front[j];
            }
        }
        sort(ratios.begin(), ratios.end());
        double best_ratio = 0.0;
        for(int i = 0; i < f*r-1; i++){
            double ratio = ratios[i+1]/ratios[i];
            best_ratio = max(ratio, best_ratio);
        }
        cout << fixed << setprecision(2);
        cout << best_ratio << endl;

    }


    return 0;
}