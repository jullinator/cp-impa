#include <bits/stdc++.h>
using namespace std;


vector<int> nums;
long long swaps = 0;

void merge(int l1, int r2){
    if(l1 == r2) return;
    int r1 = (l1+r2)/2;
    int l2 = r1+1;
    merge(l1, r1);
    merge(l2, r2);
    vector<int> res(r2-l1+1);
    int g = 0;
    int i = l1, j = l2;
    while(i <= r1 && j <= r2){
        if(nums[j] < nums[i]){
            swaps += (r1-i+1); //1 ;
            res[g] = nums[j];
            j++;
            g++;
        } else {
            res[g] = nums[i];
            i++;
            g++;
        }
    }
    while(i <= r1){
        res[g] = nums[i];
        i++;
        g++;
    }
    while(j <= r2){
        res[g] = nums[j];
        j++;
        g++;
    }
    i = l1;
    for(int x: res){
        nums[i] = x;
        i++;
    }
}

void debug(){
    for(int x: nums){
        cout << x << " ";
    }
    cout << endl;
}

int main (){
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    int n;
    cin >> n;
    nums.resize(n);
    for(int i = 0; i < n; i++){
        cin >> nums[i];
    }
    merge(0, n-1);
    //debug();
    cout << swaps << endl;
}