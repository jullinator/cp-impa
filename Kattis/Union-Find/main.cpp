#include <iostream>
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <string>
#include <vector>

using namespace std;

typedef vector<int> vi;
typedef tuple<int, int, int> iii;
typedef pair<int, int> ii;
typedef vector<iii> viii;
typedef vector<ii> vii;


int n,m;
viii edges;

class UnionFind {
    private: vi p, rank;
    public:
        UnionFind (int N){
            rank.assign(N, 0);
            p.assign(N, 0);
            for (int i=0; i<N ; i++)
                p[i] = i;
        }
        int find(int x){
            return x == p[x] ? x : p[x] = find(p[x]) ;
        }
        bool isSame(int x, int y){
            return find(x) == find(y);
        }

        void merge(int i, int j){
            if (!isSame(i, j)){
                int x = find(i), y = find(j);
                if (rank[x] < rank[y]) p[x] = y;
                else{
                    p[y] = x;
                    if(rank[x] == rank[y]) rank[x]++;
                }
            }
        }

        
};

int main (){
    int n, q;
    scanf("%d %d\n", &n, &q);
    UnionFind sets(n+1);
    for(int q0 = 0; q0 < q; q0++){
        char t;
        int v1, v2;
        scanf("%c %d %d\n", &t, &v1, &v2);
        if(t == '?'){
            if(sets.isSame(v1, v2)){
                printf("yes\n");
            } else {
                printf("no\n");
            }
        } else {
            sets.merge(v1, v2);
        }
    }
    return 0;
}