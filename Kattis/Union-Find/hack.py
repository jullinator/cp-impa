
n, q = map(int, input().split())

rank = [0]*(n+1)
parent = list(range(n+1))

def find(v):
    if v == parent[v]:
        return v
    parent[v] = find(parent[v])
    return parent[v]

def merge(p1, p2):
    if rank[p1] < rank[p2]:
        parent[p1] = p2 
    else:
        parent[p2] = p1 
        if rank[p1] == rank[p2]:
            rank[p1] += 1 

for q0 in range(q):
    t, v1, v2 = input().split()
    v1, v2 = int(v1), int(v2)
    if t == "?":
        if find(v1) == find(v2):
            print("yes")
        else:
            print("no")
    else:
        merge(find(v1), find(v2))
        

