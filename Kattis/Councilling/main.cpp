#include <cstdio>
#include <iostream>
#include <cstring>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <map>
#include <vector>
#include <set>
#include <iterator>
#define INF 2000000000

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef set<string>::iterator sit;
typedef pair<string, string> ss;

string source = "source", sink = "sink";
int num_people, max_f;                  // number of people, max_flow
map<string, map<string, int>> res;      // Residual flow
map<string, vector<string>> adj;        // Adjacency List
set<string> parties, people, clubs;     
map<string, bool> in_parties;           // To check whether a given string is actually the name of a party


int main (){
    ios_base::sync_with_stdio(false);
    int tc;
    cin >> tc;
    while(tc--){
        res.clear(); adj.clear();
        parties.clear(); people.clear(); clubs.clear();
        max_f = 0;  // reset max flow
        cin >> num_people;

        for(int i = 0; i < num_people; i++){
            string person, party;
            int num_clubs;
            cin >> person >> party >> num_clubs;

            people.insert(person);
            parties.insert(party);
            in_parties[party] = true;

            res[party][person] = 1;
            adj[party].push_back(person);
            adj[person].push_back(party);

            for(int c = 0; c < num_clubs ; c++){
                string club;
                cin >> club;
                clubs.insert(club);

                res[person][club] = 1;

                adj[person].push_back(club);
                adj[club].push_back(person);
            }
        }
        // add flow from source, sink, and clubs 
        float num_clubs = (float) clubs.size();
        int num_parties = parties.size();
        
        int max_parties = ceil(num_clubs / 2.0 - 1.0);                 // Maximum amount of council members that can be from a single party 

        // Set flow and adjacency for party-vertices
        for(sit itr = parties.begin(); itr != parties.end(); ++itr){   // try with c++11 style for(auto el: parties) later
            string party = *itr;
            res[source][party] = max_parties;
            
            adj[source].push_back(party);
            adj[party].push_back(source);
        }

        // Set flow and adjacency for club-vertices
        for(sit itr = clubs.begin(); itr != clubs.end(); ++itr){
            string club = *itr;
            res[club][sink] = 1;
            adj[club].push_back(sink);
            adj[sink].push_back(club);
        }

        // START THE ACTUAL ALGORITHM 
        vector<ss> selected_people;
        while(true){                            // Augment (bfs+flow-adding) while possible
            queue<string> q;
            q.push(source);
            map<string, string> pred;
            bool foundPath = false;
            while(!q.empty()){
                string v1 = q.front(); q.pop();
                
                if (v1 == sink){ 
                    foundPath = true;
                    break;
                }
                
                for (string v2: adj[v1]){
                    if(res[v1][v2] > 0 && pred[v2].size() == 0){
                        pred[v2] = v1;
                        q.push(v2);
                    }
                }
            }
            if(!foundPath){
                break;
            }
            string v = sink;
            string club = pred[sink];
            
            while (v != source){
                string p = pred[v];
                if(in_parties[p]){
                    selected_people.push_back(ss(v,club));  // Add this person, for output later on
                }
                res[p][v] --;
                res[v][p] ++;
                v = p;
            }
            max_f ++;
        }
        if(max_f != num_clubs){
            cout << "Impossible." << endl;
        } else{
            for(ss p: selected_people){
                cout << p.first << " " << p.second << endl;
            }
        }
        if(tc != 0){
            cout << endl;
        }

    }   
    return 0;
}