#include <cstdio>
#include <iostream>
#include <cstring>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <map>
#include <vector>
#include <set>
#include <iterator>
#include <unordered_map>
#include <unordered_set>
#define INF 2000000000

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef unordered_set<int>::iterator iit;


string source_str = "_source", sink_str = "_sink";
int source, sink;
int num_people, max_f;                          // number of people, max_flow
int res [3010][3010] ;                          // Residual flow 
int pred [3010];
unordered_set<int> parties, people, clubs;     
unordered_map <int, int> person_party;          // Which party a person belongs to

unordered_map<string, int> str_id;              // What unique int id a certain string has
string vertice_name [3010];
int str_count = 1;

// Gives every string a unique int id. Expensive(?) copy, but unsure about pointers
int get_index(string str){      
    if(str_id[str] == 0){
        str_id[str] = str_count;
        vertice_name[str_count] = str;
        str_count++;
    }
    return str_id[str];
}

int main (){
    ios_base::sync_with_stdio(false);
    int tc;
    cin >> tc;
    while(tc--){
        // Reset stuff
        memset(res, 0, sizeof res);
        parties.clear();
        people.clear();
        clubs.clear();
        person_party.clear();
        str_count = 1;
        str_id.clear();
        max_f = 0;

        cin >> num_people;

        vector<vi> adj (3010);
        vector<vi> club_adj(3010);      // Which clubs a person is a member of, user for output later on

        for(int i = 0; i < num_people; i++){
            string person, party;
            int num_clubs;
            cin >> person >> party >> num_clubs;
            int pers = get_index(person);
            int part = get_index(party);
            people.insert(pers);
            parties.insert(part);
            person_party[pers] = part;

            res[part][pers] = 1;
            adj[part].push_back(pers);
            adj[pers].push_back(part);

            for(int c = 0; c < num_clubs ; c++){
                string club_str;
                cin >> club_str;
                int club = get_index(club_str);
                clubs.insert(club);

                res[pers][club] = 1;

                adj[pers].push_back(club);
                club_adj[pers].push_back(club); // For output
                adj[club].push_back(pers);
            }
        }
        // add flow from source, sink, and clubs
        source = get_index(source_str);
        sink = get_index(sink_str);
        
        int num_clubs = clubs.size();
        int num_parties = parties.size();
        
        int max_parties = num_clubs/2;                // Maximum amount of council members that can be from a single party 
        if(!(num_clubs & 1)) {                        // If even, minus one more
            max_parties--;
        }

        // Set flow and adjacency for party-vertices
        for(iit itr = parties.begin(); itr != parties.end(); ++itr){   // try with c++11 style for(auto el: parties) later
            int party = *itr;
            res[source][party] = max_parties;
            
            adj[source].push_back(party);
            adj[party].push_back(source);
        }

        // Set flow and adjacency for club-vertices
        for(iit itr = clubs.begin(); itr != clubs.end(); ++itr){
            int club = *itr;
            res[club][sink] = 1;
            adj[club].push_back(sink);
            adj[sink].push_back(club);
        }

        // START THE ACTUAL ALGORITHM 
        while(true){                            // Augment (bfs+flow-adding) while possible
            queue<int> q;
            q.push(source);
            memset(pred, -1, sizeof pred);
            bool foundPath = false;
            while(!q.empty()){
                int v1 = q.front(); q.pop();
                
                if (v1 == sink){ 
                    foundPath = true;
                    break;
                }
                
                for (int v2: adj[v1]){
                    if(res[v1][v2] > 0 && pred[v2] == -1){
                        pred[v2] = v1;
                        q.push(v2);
                    }
                }
            }
            if(!foundPath){
                break;
            }
            int v = sink;
            while (v != source){
                int p = pred[v];
                res[p][v] --;
                res[v][p] ++;
                v = p;
            }
            max_f ++;
        }
        // cout << max_f << " " << num_clubs << " " << max_parties; // debug
        if(max_f != num_clubs){
            cout << "Impossible." << endl;
        } else{
            int count = 0;
            for(iit itr = people.begin(); itr != people.end(); ++itr){
                int person = *itr;
                int party = person_party[person];
                int selected_club = -1;
                if(res[party][person] != 0) continue; //This person was not selected
                for(int club: club_adj[person]){
                    if(res[person][club] == 0){
                        selected_club = club;
                        break;
                    }
                }
                if(selected_club == -1) continue;   // No flow to club, should not happen, but whatever
                cout << vertice_name[person] << " " << vertice_name[selected_club] << endl;
                count ++;
                if(count == max_f) break;
            }
        }
        if(tc != 0){
            cout << endl;
        }

    }   
    return 0;
}