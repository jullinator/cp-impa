nodes = []
R, C = map(int, raw_input().split())
for r0 in xrange(R):
    nodes.append(raw_input())



parents = range(R*C)
ranks = [0]*(R*C)


def find(v1):
    while parents[v1] != v1:
        v1, parents[v1] = parents[v1], parents[parents[v1]]
    return v1

def union(v1, v2):
    v1, v2 = find(v1), find(v2)
    r1, r2 = ranks[v1], ranks[v2]
    if r1 < r2:
        parents[v1] = v2 
    else:
        parents[v2] = v1 
        if r1 == r2:
            ranks[v1] += 1 

for r in xrange(R):
    for c in xrange(C):
        y1, x1 = r + 1, c
        if y1<R and nodes[y1][x1] == nodes[r][c]:
            union(y1*C + x1, r*C + c)
        y2, x2 = r, c+1
        if x2<C and nodes[y2][x2] == nodes[r][c]:
            union(y2*C + x2, r*C + c)
                


T = int(raw_input())
for t0 in range(T):
    r1, c1, r2, c2 = map(lambda x2: int(x2)-1, raw_input().split())
    if nodes[r1][c1] != nodes[r2][c2]:
        print "neither"
        continue
    
    if find(r1*C + c1) == find(r2*C + c2):
        if nodes[r1][c1] == "0":
            print "binary"
        else:
            print "decimal"
    else:
        print "neither"
