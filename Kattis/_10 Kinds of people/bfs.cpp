#include <bits/stdc++.h>
#define MAX_V 1001

using namespace std;

int R, C, T;
char nodes[MAX_V][MAX_V];
int component[MAX_V*MAX_V];
int ci = 0;
int dr [] = {-1, 0, 1, 0};
int dc [] = {0, 1, 0, -1};

void bfs (int r, int c) {
    queue<int> q;
    int source = r*C + c;
    q.push(source);
    component[source] = ci;
    while(!q.empty()){
        int v = q.front();
        q.pop();
        for (int i = 0; i < 4; i++){
            int y = (v/C) + dr[i];
            int x = (v%C) + dc[i];
            if (y>= 0 && y < R && x >= 0 && x < C && component[y*C + x] == -1 && nodes[r][c] == nodes[y][x]){
                int v2 = y*C + x;
                component[v2] = ci;
                q.push(v2);
            }
        }
    }

}

int main (){
    memset(component, -1, sizeof(component));
    cin >> R >> C;
    for(int r = 0; r < R; r++)
        for (int c = 0; c < C; c++)
            cin >> nodes[r][c];
        
            
    cout << component[0] << endl;
    for(int r = 0; r < R; r++){
        for (int c = 0; c < C; c++){
            cout << component[r*C + c] << " ";
            if(component[r*C + c] == -1){
                bfs(r, c);
                ci++;
            }
            
        }
        cout << endl;
    }
    

    cin >> T;
    for (int t0 = 0; t0 < T; t0++){
        int r1, c1, r2, c2;
        cin >> r1 >> c1 >> r2 >> c2;
        --r1; --c1; --r2; --c2;
        if(component[r1*C + c1] == component[r2*C + c2]){
            if(nodes[r1][c1] == '0'){
                cout << "binary\n";
            } else {
                cout << "decimal\n";
            }
        } else {
            cout << "neither\n";
        }
    }
            
}