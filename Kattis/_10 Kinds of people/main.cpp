#include <bits/stdc++.h>
#define MAX_V 1001
using namespace std;




int nodes [MAX_V][MAX_V];
int parent [MAX_V*MAX_V];
int ranks [MAX_V*MAX_V];
int R, C, T;

int find(int v1){
    while(v1 != parent[v1]){
        int prev = v1;
        v1 = parent[v1];
        parent[prev] = parent[v1];
    }
    return v1;
}

void merge(int v1, int v2){
    v1 = find(v1);
    v2 = find(v2);
    if (v1 == v2)
        return;
    int r1 = ranks[v1], r2 = ranks[v2];
    if(r1 < r2){
        parent[v1] = v2;
    } else {
        parent[v2] = v1;
        if (r1 == r2)
            ranks[v1]++;
    }
}


int main (){
    scanf("%d %d\n", &R, &C);
    for(int r=0; r<R; r++)
        for(int c=0; c<C; c++)
            scanf("%1d", &nodes[r][c]);


    for(int r=0; r<R; r++)
        for(int c=0; c<C; c++)
            parent[r*C + c] = r*C + c;
    
    memset(ranks, 0, sizeof(ranks));
    for(int r=0; r<R; r++){
        for(int c=0; c<C; c++){
            int y1 = r+1, x1 = c;
            if (y1 < R && nodes[y1][x1] == nodes[r][c])
                merge(r*C + c, y1*C + x1);
            int y2 = r, x2 = c+1;
            if (x2 < C && nodes[y2][x2] == nodes[r][c])
                merge(r*C + c, y2*C + x2);
        }
    }
    
    scanf("%d", &T);
    for (int t0=0; t0 < T; t0++){
        int r1, c1, r2, c2;
        scanf("%d%d%d%d", &r1, &c1, &r2, &c2);
        r1--; c1--; r2--; c2--;
        int type1 = nodes[r1][c1], type2 = nodes[r2][c2];
        if (type1 != type2){
            printf("neither\n");
            continue;
        }
        if(find(r1*C + c1) == find(r2*C + c2)){
            if (type1 == 0)
                printf("binary\n");
            else 
                printf("decimal\n");
        } else {
            printf("neither\n");
        }
    }




    return 0;
}
