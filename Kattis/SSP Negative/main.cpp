/*
* 
*
*
*/
#include <iostream>
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <map>

#define INF 2000000000

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;

int main (){
    ios::sync_with_stdio(false);
    int tc = 0;
    while(true){
        int n, m, q, s;
        cin >> n >> m >> q >> s;
        if(!(n||m||q||s)) break;
        if(tc!=0) cout << endl; tc++;

       
        vector<vii> adj(n);

        for(int m1 = 0; m1 < m; m1++){
            int v1, v2, w;
            cin >> v1 >> v2 >> w; 
            adj[v1].push_back(ii(w, v2));
        }

        vi dist(n, INF); dist[s] = 0;
        for(int i = 0; i <n-1; i++)
            for(int u = 0; u < n; u++)
                for(ii p1: adj[u]){
                    int v2 = p1.second, w2 = p1.first;
                    dist[v2] = min(dist[v2], dist[u] + w2);
                }

        map<int, bool> neg;

        
        for(int i = 0; i <n-1; i++)
            for(int u = 0; u < n; u++)
                for(ii p1: adj[u]){
                    int v2 = p1.second, w2 = p1.first;
                    if(dist[v2] > dist[u] + w2){
                        dist[v2] = dist[u] + w2;
                        neg[v2] = true;
                        neg[u] = true;
                        //cout << v2 << " Neg "<< u << endl;
                    }
                }
        vector<map<int, bool>> neg_reach;
        for(pair<int,bool> p: neg){
                if(!p.second) continue; //unneccess
                // do bfs to see which are reachable from here
                map<int, bool> reach;
                int n1 = p.first;
                queue<int> q; q.push(n1); reach[n1] = true;
                while(!q.empty()){
                    int v1 = q.front(); q.pop();
                    for(ii p1: adj[v1]){
                        int v2 = p1.second;
                        if(reach[v2]) continue;
                        reach[v2] = true;
                        q.push(v2);
                    }
                }
                neg_reach.push_back(reach);             

        }
        

        for(int q1 = 0; q1 < q ; q1++){
            int t;
            cin >> t;
            if(dist[t] == INF) cout << "Impossible" << endl;
            else {
                bool is_inf = false;
                for(map<int,bool> reach: neg_reach){
                    if(reach[t]){
                        is_inf=true;
                        break;
                    }
                }
                if(is_inf) cout << "-Infinity" << endl;
                else       cout << dist[t] << endl;
            }
        }


    }
    return 0;
}