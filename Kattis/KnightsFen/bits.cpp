#include <bits/stdc++.h>
using namespace std;

int DX [] = {-1, -2, -2, -1, 1, 2, 2, 1};
int DY [] = {2, 1, -1, -2, -2, -1, 1, 2};
int target = 0b011000000010000110001111011111;

int get_row(int x){return x/5;}
int get_col(int x){return x%5;}
int clear_bit(int &x, int bit){x = x & ~(1<<bit); }
int set_bit(int &x, int bit){x = x | (1<<bit); }
bool is_on(int x, int bit){return (x & (1<<bit)) > 0; }
void debug(int x){
    string s = bitset<32>(x).to_string();
    string out = "";
    int empty = (x>>25);
    for(int i=31; i>3; i--){
        if(31-i == empty){
            out += "_";
        } else{
            out += s[i];
        }
        if(out.length() >= 5){
            cout << out << endl;
            out = "";
        }
    }
    cout << "\n";
    
}

int main(){
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    int tc;
    cin >> tc;
    for(int tc0= 0; tc0 < tc; tc0++) {
        int start = 0;
        int empty_idx = 0;
        for(int i = 0; i<25; i++){
            if(i%5 == 0) cin.ignore();
            char c;
            cin.get(c);
            int x;
            if(c == ' '){
                empty_idx = i;
                x = 0;
            } else {
                x = c - '0';
            }
            start += x << i;
        }
        start += (empty_idx << 25);
        //debug(start);

        int ans = 20; 
        unordered_map<int, int> dist;
        unordered_map<int, int> end_dist;
        dist[start] = 0;
        end_dist[target] = 0;
        queue<int> q;
        queue<int> q_end;
        q.push(start);

        while(!q.empty()){
            int board = q.front(); q.pop();
            
            if(board == target){
                ans = dist[board];
                break;
            } else if (end_dist.find(board) != end_dist.end()){
                ans = dist[board] + end_dist[board];
                break;
            }
            if(dist[board] >= 5) continue;
            //debug(board);
            int empty = (board>>25);
            int y = get_row(empty), x = get_col(empty); 
            for(int i = 0; i < 8; i++){
                int x2 = x + DX[i], y2 = y + DY[i];
                if(x2 >= 0 && x2 < 5 && y2 >= 0 && y2 < 5){
                    int empty2 = y2*5 + x2;
                    int board2 = board;
                    is_on(board2, empty2) ? set_bit(board2, empty) : clear_bit(board2, empty);
                    clear_bit(board2, empty2);
                    board2 = (board2 - (empty<<25)) + (empty2<<25); // Set the new empty idx
                    if(dist.find(board2) != dist.end()) {
                        //cout << "exists" << endl;
                        continue;
                    }
                    dist[board2] = dist[board] + 1;
                    q.push(board2);
                }
            }
        }
        q_end.push(target);
        while(!q_end.empty() && ans == 20){
            int board = q_end.front(); q_end.pop();
            
            if(board == start){
                ans = end_dist[board];
                break;
            } else if(dist.find(board) != dist.end()){
                ans = end_dist[board] + dist[board];
                break;
            }
            if(end_dist[board] >= 5) continue;
            //debug(board);
            int empty = (board>>25);
            int y = get_row(empty), x = get_col(empty); 
            for(int i = 0; i < 8; i++){
                int x2 = x + DX[i], y2 = y + DY[i];
                if(x2 >= 0 && x2 < 5 && y2 >= 0 && y2 < 5){
                    int empty2 = y2*5 + x2;
                    int board2 = board;
                    is_on(board2, empty2) ? set_bit(board2, empty) : clear_bit(board2, empty);
                    clear_bit(board2, empty2);
                    board2 = (board2 - (empty<<25)) + (empty2<<25); // Set the new empty idx
                    if(end_dist.find(board2) != end_dist.end()) {
                        //cout << "exists" << endl;
                        continue;
                    }
                    end_dist[board2] = end_dist[board] + 1;
                    q_end.push(board2);
                }
            }
        }
        if(ans <= 10){
            cout << "Solvable in " << ans << " move(s).\n";
        } else {
            cout << "Unsolvable in less than 11 move(s).\n";
        }

    }

}