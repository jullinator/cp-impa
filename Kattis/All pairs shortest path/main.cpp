/*
* 
* All pairs shortest path - O(v^3) 
*
*/
#include <iostream>
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <string>
#include <vector>
#include <map>

using namespace std;
#define INF 1000000

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;





int main (){
    ios::sync_with_stdio(false);
    int tc = 0;
    while(true){
        int n, m, q;
        cin >> n >> m >> q;
        if(!(n || m || q)) break;
        if (tc!=0) cout << endl; tc++;

        int dist [155][155];
        for(int i = 0; i < 155; i++)
            for(int j = 0; j < 155; j++) dist[i][j] = INF;

        for (int i = 0; i < n; i++) dist[i][i] =  0; // Zero distance to itself

        for (int m1 = 0; m1 < m; m1++){
            int v1, v2, w;
            cin >> v1 >> v2 >> w;
            dist[v1][v2] = min(dist[v1][v2], w);
        }



        for(int k = 0; k < n; k++)
            for(int i= 0; i < n; i++)
                for(int j = 0; j<n; j++){
                    dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j]);
                    //cout << dist[i][j] << " " << i <<  " " << j << endl;
                }
                    
                    
        // Check negative cycle 
        vii neg_cycle;
        for(int k = 0; k < n; k++)
            for(int i= 0; i < n; i++)
                for(int j = 0; j<n; j++){
                    if (dist[i][k] + dist[k][j] < dist[i][j] ){
                        neg_cycle.push_back(ii(i, j));
                    } 
                }
                    
        

        for (int q1 = 0; q1<q; q1++){
            int s, t;
            cin >> s >> t;
            if (dist[s][t] == INF) cout << "Impossible" << endl;

            else{
                bool has_cycle = false;
                for(ii p1: neg_cycle){
                    if(s == p1.first && t == p1.second){
                        has_cycle = true; break;
                    } 
                }
                if(has_cycle) cout << "-Infinity" << endl;
                else          cout << dist[s][t] << endl;
            }
        }
    }

    return 0;
}