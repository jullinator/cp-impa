/*
*   Very optimistic assumptions:
*   1. Towers are given in sorted order x, y. [CONFIRMED, sort of]
*   2. Possible to save one switch if line goes through an intersection [CONFIRMED]
*   3. 
*/

// Really bad, fails cross.in, gives 1 irrespective of whether or not it is paralell [My bad, hadn't implemented the "areParalell" function yet lol]
#include <cstdio>
#include <cstring>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#include <unordered_map>
#include <set>
#define INF 2000000000
#define EPS 1e-9

using namespace std;

typedef pair<int, int> ii;
typedef pair<double, int> di;
typedef vector<int> vi;
typedef vector<ii> vii;

int B, C, R, Q;
string IMPOSSIBLE = "Impossible.";



struct point {
    double x, y;
    //point(double _x, double _y): x(_x), y(_y) {}
};

struct line { 
    double a, b, c;
    line (): a(1), b(1), c(0){}
};

void pointsToLine(point p1, point p2, line &l){
    // Check if k (kx) is inf
    if (fabs(p1.x - p2.x) < EPS){
        l.a = 1; l.b = 0; l.c = -p1.x;
    } else {
        // FUCKED, p1 has to be < p1 for c to be correct
        l.a = - (double) (p1.y - p2.y) / (p1.x - p2.x);
        l.b = 1.0;
        l.c = - (double) l.a*p1.x - p1.y;  // Change this so that c will be correct(not necessarry for this one)
    }
}

bool areParallel (line l1, line l2){
    return fabs(l1.a - l2.a) < EPS && fabs(l1.b -l2.b) < EPS;
}

int main (){
    int tc = 0;
    
    while(true){
        scanf("%d %d %d %d\n", &B, &C, &R, &Q);
        if(Q==0) break;
        tc++;
        
        vector<vii> adj(C+1); // (switches to be made, v2)
        vector<ii> city_cell(C+1);
        vector<point> city_cord(C+1);

        double startX, startY;
        scanf("%lf %lf\n", &startX, &startY);

        double prevX = startX, prevY = startY;
        double difX = 0, difY = 0;
        int rows = 1, cols = 1;

        for(int i = 1; i < B; i++){
            double x, y;
            scanf("%lf %lf\n", &x, &y);
            difX = max(difX, x - prevX); prevX = x;
            difY = max(difY, y- prevY); prevY = y;
            if(prevX > x) rows++;
            if(i==0){
                startX = x; startY = y;
            }
        }
        cols = B/rows;
        startX -= difX/2.0; startY -= difY / 2.0;

        
        
        
        // Cities
        for(int i = 0; i < C; i++){
            double x, y;
            
            scanf("%lf %lf\n", &x, &y);
            point p{x,y};
            city_cord[i+1] = p;
            int yCell = y / difY;
            int xCell = x / difX;
            city_cell[i+1] = ii(xCell, yCell);
            

        }

        // Roads- Later optimization is DP for road weight.
        for(int i = 0; i < R; i++){
            
            int v1, v2;
            scanf("%d %d\n", &v1, &v2);
            ii cell1 = city_cell[v1], cell2 = city_cell[v2];
            
            int dist = abs(cell1.first - cell2.first) + abs(cell1.second - cell2.second);
            line l1 = {};
            pointsToLine(city_cord[v1], city_cord[v2], l1);


            // only checks possible (avoid error). For cells (2,0) - (0,2), only x:(1,2) y:(1,2) (4 combinations)

            for(int x = min(cell1.first, cell2.first) + 1; x <= max(cell1.first, cell2.first); x++){            // Dirty as shit
                for(int y = min(cell1.second, cell2.second) + 1; y <= max(cell1.second, cell2.second); y++){    // same
                    point crossing{startX+ x*difX, startY+ y*difY};
                    line l2 = {};
                    pointsToLine(city_cord[v1], crossing, l2);
                    if(areParallel(l1, l2)){
                        dist--;
                    }
                }
            }
            adj[v1].push_back(ii(dist, v2));
            adj[v2].push_back(ii(dist, v1));
        }

        printf("Case %d:\n", tc);
        // Queries
        for(int i = 0; i < Q; i++){
            int s, t;
            scanf("%d %d\n", &s, &t);

            priority_queue <ii, vii, greater<ii>> pq;
            vi dist(C+1, INF);

            pq.push(ii(0, s));
            dist[s] = 0;
            
            while(!pq.empty()){
                ii p1 = pq.top(); pq.pop();
                int dist1 = p1.first, v1 = p1.second;
                if(v1 == t) break;
                if(dist1 > dist[v1]) continue;
                for (ii edge: adj[v1]){
                    int dist2 = edge.first + dist1;
                    int v2 = edge.second;
                    if(dist2 < dist[v2]){
                        dist[v2] = dist2;
                        pq.push(ii(dist2, v2));
                    }
                }
            }

            if(dist[t] == INF){
                printf("Impossible.\n");
            } else {
                printf("%d\n", dist[t]);
            }

        }
    }
    return 0;
}