/*
*
*/
#include <cstdio>
#include <cstring>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#include <unordered_map>
#define INF 2000000000
#define EPS 1e-9

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int B, C, R, Q;
string IMPOSSIBLE = "Impossible.";

unordered_map <double, int> x_ind, y_ind;
int cur_x_ind, cur_y_ind;
void set_index(double xcord, double ycord, int &x, int &y){
    if(x_ind.find(xcord) == x_ind.end()){
        x_ind[xcord] = cur_x_ind;
        cur_x_ind ++;
    }
    x = x_ind[xcord];

    if(y_ind.find(ycord) == y_ind.end()){
        y_ind[ycord] = cur_y_ind;
        cur_y_ind ++;
    }
    y = y_ind[ycord];
}


int main (){
    int tc = 0;
    while(true){
        cur_x_ind = 0, cur_y_ind = 0;
        scanf("%d %d %d %d\n", &B, &C, &R, &Q);
        if(Q==0) break;
        tc++;

        for(int i = 0; i < B; i++){
            double xcord, ycord;
            int x, y;
            scanf("%lf %lf\n", &xcord, &ycord);
            set_index(xcord, ycord,x,y);
            
        }

        for(int i = 0; i < C; i++){

        }

        for(int i = 0; i < R; i++){

        }

        printf("Case %d:\n", tc);
        
        for(int i = 0; i < Q; i++){

        }
    }
    return 0;
}