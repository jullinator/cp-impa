/*
*
*/
#include <cstdio>
#include <cstring>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#include <unordered_map>
#include <set>
#define INF 2000000000
#define EPS 1e-9

using namespace std;

typedef pair<int, int> ii;
typedef pair<double, int> di;
typedef vector<int> vi;
typedef vector<ii> vii;

int B, C, R, Q;
string IMPOSSIBLE = "Impossible.";



struct point {
    double x, y;
    point(double _x, double _y): x(_x), y(_y) {}
};


int main (){
    int tc = 0;
    while(true){
        scanf("%d %d %d %d\n", &B, &C, &R, &Q);
        if(Q==0) break;
        tc++;

        set <double> gsm_x, gsm_y;
        vector<vii> adj(C+1); // (switches to be made, v2)

        // GSM Stations
        for(int i = 0; i < B; i++){
            double x, y;
            scanf("%lf %lf\n", &x, &y);
            gsm_x.insert(x);
            gsm_y.insert(y);
        }

        // Find width and height of tower field
        

        // Cities
        for(int i = 0; i < C; i++){
            double x, y;
            scanf("%lf %lf\n", &x, &y);
        }

        // Roads- Later optimization is DP for road weight.
        for(int i = 0; i < R; i++){
            int v1, v2;
            scanf("%d %d\n", &v1, &v2);
        }

        // Queries
        for(int i = 0; i < Q; i++){
            int v1, v2;
            scanf("%d %d\n", &v1, &v2);
        }
    }
    return 0;
}