#include <bits/stdc++.h>
#define INF 1000000000

using namespace std;

struct Edge{
    int to, capacity, flow;
    Edge backEdge;
};

int main(){
    ios_base.sync_with_stdio(false); cin.tie(null);
    int testCase = 1;
    Edge e = {1,0,0};
    Edge e2 = {2,0,0};
    e.backEdge = &e2;
    e.backEdge.to = 4;
    cout << e2.to;
    while(true){
        int numRobots, numHoles;
        cin >> numRobots;
        if (numRobots == 0) break;
        cout << "Scenario " << testCase++ << endl;

    }
}
