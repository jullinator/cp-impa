/*
* Edmonds karp with dijkstra (instead of bfs)
* O(E^3logV) => TLE like fuck, but let's try it anyways
* Alternative is push-relabel (O(V^3*d)), or perhaps Dinics (E*V^2*d) [d = dijkstra additional time]
* .. Max_flow works, min cost does not
*/
#include <cstdio>
#include <cstring>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#define INF 2000000000

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int n, m, s, t, m_flow, flow = 0, total_cost = 0;
int weights[251][251], res[251][251], pred[251];

void augment(int v, int minEdge){
    if(v == s) {
        flow = minEdge;
        return;
    }
    int p = pred[v];
    augment(p, min(minEdge, res[p][v]));
    res[p][v] -= flow;
    res[v][p] += flow;

}

int main (){
    scanf("%d %d %d %d\n", &n, &m, &s, &t);
    vector<vi> adj(n);
    for(int i = 0; i < m ; i++){
        int v1, v2, c, w;
        scanf("%d %d %d %d\n", &v1, &v2, &c, &w);
        adj[v1].push_back(v2);
        adj[v2].push_back(v1);
        res[v1][v2] = c;
        weights[v1][v2] = w;
        weights[v2][v1] = -w;
    }
    // Augment path whilst such exists
    while(true){
        priority_queue <ii, vii, greater<ii>> pq;
        flow = 0;
        pq.push(ii(0, s));
        memset(pred, -1, sizeof pred);
        vi dist (n, INF);
        vector<bool> visited(n);
        while(!pq.empty()){
            ii p1 = pq.top(); pq.pop();
            int dist1 = p1.first, v1 = p1.second;
            if(v1 == t){
                break;
            }
            if(dist1 > dist[v1]) continue;
            visited[v1] = true;
            for(int v2: adj[v1]){
                int dist2 = dist1 + weights[v1][v2];
                if( res[v1][v2] > 0 && dist2 < dist[v2] && v2 != s && !visited[v2]){
                    pred[v2] = v1;
                    dist[v2] = dist2;
                    pq.push(ii(dist2, v2));
                }
            }
        }
        
        if(pred[t] == -1) break; // Doesn't need to check flow > 0
        augment(t, INF);
        if(flow == 0) break;
        m_flow += flow;
        total_cost += dist[t]*flow;
    }
    printf("%d %d\n", m_flow, total_cost);
    return 0;
}