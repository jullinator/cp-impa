/*
*
*/
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#include <cstring>
#define INF 2000000000
#define MAX_V 501

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

vi adj [MAX_V];
int res [MAX_V][MAX_V];
int N, M, f = 0, max_f = 0, s, t;
vi p;
bool edge_exists [MAX_V][MAX_V];


void augment(int v, int minEdge){
    if(v == s) {
        f = minEdge; return;
    } else if(p[v] != -1){
        augment(p[v], min(res[p[v]][v], minEdge));
        res[p[v]][v] -= f; res[v][p[v]] += f;
    }

}

int main (){
    scanf("%d %d %d %d\n", &N, &M, &s, &t);
    vii edges(M);
    memset(res, 0, sizeof res);
    memset(edge_exists, false, sizeof edge_exists);
    for(int m = 0; m < M; m++){
        int v1, v2, c;
        scanf("%d %d %d\n", &v1, &v2, &c);
        res[v1][v2] += c;
        if(!(edge_exists[v1][v2] || edge_exists[v2][v1])){  // To account for multiple edges
            adj[v1].push_back(v2);
            adj[v2].push_back(v1);
            edges.push_back(ii(v1, v2));
            edge_exists[v1][v2] = true;
        }
    }

    // bfs
    while(true){
        queue<int> q; q.push(s);
        vi dist(N, INF); dist[s] = 0;
        p.assign(N, -1);
        int v1;
        while(!q.empty()){
            v1 = q.front(); q.pop();
            if (v1 == t) break;
            for (int v2: adj[v1]){
                if(res[v1][v2] > 0 && dist[v2]==INF){
                    dist[v2] = dist[v1] + 1;
                    p[v2] = v1;
                    q.push(v2);
                }
            }
        }
        if(v1 != t) break;
        augment(t, INF);
        if(f == 0) break; // no augmenting path
        max_f += f;
    }
    // Get the sorted path of used edges
    vii path;
    for(ii edge: edges){
        int v1 = edge.first, v2 = edge.second;
        if(res[v2][v1] > 0){ // Check if there is capacity on the reversed edge
            path.push_back(ii(v1, v2));
        }
    }

    int m2 = path.size();
    sort(path.begin(), path.end(), [](ii e1, ii e2 ){
        if(e1.first == e2.first) return e1.second < e2.second;
        return e1.first < e2.first;
    });
    printf("%d %d %d\n", N, max_f, m2);
    for(ii edge: path){
        int v1 = edge.first, v2 = edge.second;
        printf("%d %d %d\n", v1, v2, res[v2][v1]);
    }
    return 0;
}