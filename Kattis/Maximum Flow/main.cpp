/*
*
*/
#include <iostream>
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#define INF 2000000000

using namespace std;

typedef pair<int, int> ii;
typedef tuple<int, int, int> i3;
typedef vector<i3> vi3;
typedef vector<int> vi;
typedef vector<ii> vii;


int n, m, s, t, flow = 0;
vi adj[501];
ii cap[501][501];



int bfs(){
    queue<int> q; q.push(s);
    vi pred (n, -1);
    while(!q.empty()){
        int v1 = q.front(); q.pop();
        if(v1 == t) break;
        for(int v2: adj[v1]){
            
            int f = cap[v1][v2].first, capacity = cap[v1][v2].second;
            if(pred[v2] == -1 && f < capacity && v2 != s){
                pred[v2] = v1;
                q.push(v2);
            }
        }
    }
    // Augment path 
    if(pred[t] != -1){  // Has an augmenting path
        int f = INF;
        int v2 = t;
        while(true){
            int v1  = pred[v2];
            f = min(f, cap[v1][v2].second - cap[v1][v2].first);
            v2 = v1;
            if(v1 == s) break; 
        }
        v2 = t;
        
        while(true){
            int v1  = pred[v2];
            cap[v1][v2].first += f;
            cap[v2][v1].first -= f;
            v2 = v1;
            if(v1 == s) break; 
        }
        return f;
    }
    return 0;
}

int main (){
    ios::sync_with_stdio(false);
    cin >> n >> m >> s >> t;
    vii edges(m);
    for(int m1 = 0; m1 < m; m1++){
        int v1, v2, c;
        cin >> v1 >> v2 >> c;
        edges.push_back(ii(v1, v2));
        adj[v1].push_back(v2);
        adj[v2].push_back(v1);
        cap[v1][v2] = ii(0, c);
        cap[v2][v1] = ii(c, c);

    }

    while(true){
        int f = bfs();
        flow += f;
        //cout << f << endl;
        if(f == 0) break;   // No augmenting path
    }
    vii path;

    for(ii edge: edges){
        int v1 = edge.first, v2 = edge.second;
        if(cap[v1][v2].first > 0){
            path.push_back(ii(v1, v2));
        }
    }

    int m2 = path.size();
    sort(path.begin(), path.end(), [](ii e1, ii e2 ){
        if(e1.first == e2.first) return e1.second < e2.second;
        return e1.first < e2.first;
    });

    cout << n << " " << flow << " " << m2 << endl;
    for(ii edge: path){
        cout << edge.first << " " << edge.second << " " << cap[edge.first][edge.second].first << endl;
    }
    //output flow

    
    return 0;
}