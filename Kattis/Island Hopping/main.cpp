/*
* MST..?
*/
#include <cstdio>
#include <cstring>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#define INF 2000000000

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef pair<float,float> ff;
typedef pair<double, int> di; // dist, vertice

int main (){
    int tc;
    scanf("%d\n", &tc);
    while (tc--){
        int n;
        scanf("%d\n", &n);
        vector<ff> islands(n); 
        for(int i = 0; i < n ; i++){
            float x, y;
            scanf("%f %f\n", &x, &y);
            islands[i] = ff(x, y); 
        }
        double cost = 0.0;
        vector<double> dist(n, (double) INF); 
        vector<bool> visited(n);
        priority_queue <di, vector<di>, greater<di>> pq;
        pq.push(di(0.0, 0));
        dist[0] = 0.0;
        while(!pq.empty()){
            di p = pq.top(); pq.pop();
            double weight1 = p.first;
            int v1 = p.second;
            if(visited[v1]) continue;
            visited[v1] = true;
            cost += weight1;
            ff cord1 = islands[v1];
            float x1 = cord1.first, y1 = cord1.second;
            for(int v2 = 0; v2 < n; v2++){
                if (v2 == v1) continue;
                ff cord2 = islands[v2];
                float x2 = cord2.first, y2 = cord2.second;
                double weight2 = sqrt(pow(y2-y1, 2) + pow(x2-x1, 2));
                if (weight2 < dist[v2] && !visited[v2]){
                    dist[v2] = weight2;
                    pq.push(di(weight2, v2));
                }
            }
        }
        printf("%.3f\n", cost);
    }
    return 0;
}