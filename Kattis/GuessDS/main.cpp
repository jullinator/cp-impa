#include <bits/stdc++.h>

using namespace std;

int main (){

    ios_base::sync_with_stdio(false);
    int n;
    while(cin >> n){
        queue<int> q;
        priority_queue<int> pq;
        stack<int> s;
        bool qt = true, pqt = true, st = true;
        
        for (int i = 0; i < n; i++){
            int op, x;
            cin >> op >> x;
            if(op == 1){
                q.push(x);
                pq.push(x);
                s.push(x);
            } else if(pq.size() == 0){
                qt = false; pqt = false; st = false;
            } else {
                int x1 = q.front(); q.pop();
                qt = qt && x1 == x;
                int x2 = pq.top(); pq.pop();
                //cout << x2 << endl;
                pqt = pqt && x2 == x;
                int x3 = s.top(); s.pop();
                //cout << "pq: " << "op: " << op << " " << x2 << " " << x << endl;
                st = st && x3 == x;
            }
        }
        if( (qt && pqt) || (qt && st) || (pqt && st)){
            cout << "not sure\n";
        } else if(qt){
            cout << "queue\n";
        } else if(pqt){
            cout << "priority queue\n";
        } else if(st){
            cout << "stack\n";
        } else {
            cout << "impossible\n";
        }
    }
    return 0;
}