import math 
from collections import deque

class Edge(object):
    def __init__(self, to, capacity, flow=0):
        self.to = to
        self.capacity = capacity
        self.flow = flow 
        self.back_edge = None 

class Dinic(object):
    def __init__(self, size):
        self.levels = [-1] * size
        self.current_edges = [0] * size  
        self.adj = [[] for i in xrange(size)]

    def reset(self):
        for seq in self.adj:
            for edge in seq:
                edge.flow = 0

    def add_edge(self, _from, to, capacity):
        e1, e2 = Edge(to, capacity), Edge(_from, 0)
        e1.back_edge, e2.back_edge = e2, e1 
        self.adj[_from].append(e1)
        self.adj[to].append(e2)

    def max_flow(self, source, sink):
        self.source = source 
        self.sink = sink 
        mf = 0
        while self.bfs():
            self.current_edges = [0]*len(self.current_edges)
            flow = self.dfs(source, float('inf'))
            while flow != 0:
                mf += flow 
                flow = self.dfs(source, float('inf'))
        return mf 
    
    def bfs(self):
        self.levels = [-1] * len(self.levels)
        q = deque([self.source])
        self.levels[self.source] = 0
        while q:
            v = q.popleft()
            for edge in self.adj[v]:
                if self.levels[edge.to] == -1 and edge.flow < edge.capacity:
                    q.appendleft(edge.to)
                    self.levels[edge.to] = self.levels[v] + 1 
        return self.levels[self.sink] != -1 
    
    def dfs(self, v, flow):
        if flow == 0: return 0
        if v == self.sink: return flow 
        while self.current_edges[v] < len(self.adj[v]):
            idx = self.current_edges[v]
            edge = self.adj[v][idx]
            if self.levels[edge.to] == self.levels[v]+1:
                min_flow = self.dfs(edge.to, min(flow, edge.capacity-edge.flow))
                if min_flow > 0:
                    edge.flow += min_flow 
                    edge.back_edge.flow -= min_flow 
                    return min_flow
            self.current_edges[v] += 1
        return 0
        



test_case = 1
while True:
    num_robots = int(raw_input())
    if num_robots == 0:
        break 
    print "Scenario %d" % (test_case)
    test_case += 1
    robots = []
    for i in xrange(num_robots):
        robots.append([float(val) for val in raw_input().split()])
    num_holes = int(raw_input())
    holes = []
    for i in xrange(num_holes):
        holes.append([float(val) for val in raw_input().split()])
    source = num_holes + num_robots
    sink = source + 1 
    graph = Dinic(sink+1)
    for i in xrange(num_robots):
        graph.add_edge(source, i, 1)
    for i in xrange(num_holes):
        graph.add_edge(i+num_robots, sink, 1)

    sec = 5
    has_edge = [[False for j in xrange(num_holes)] for i in xrange(num_robots)]
    while sec <= 20:
        if sec != 5:
            graph.reset()
        max_dist = sec*10
        for i in xrange(num_robots):
            for j in xrange(num_holes):
                x1, y1 = robots[i]
                x2, y2 = holes[j]
                dist = math.hypot(x1-x2, y1-y2)
                if dist <= max_dist and not has_edge[i][j]:
                    has_edge[i][j] = True
                    graph.add_edge(i, num_robots+j, 1)
        flow = graph.max_flow(source, sink)
        print "In %d seconds %d robot(s) can escape" % (sec, flow)
        sec *= 2
    print ""