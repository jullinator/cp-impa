/*
* Difficult to be honest, but mathematically it is quite easy
* m choose n -> win = combinations that add up to t : loss = the rest
*
*/
#include <iostream>
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <string>
#include <vector>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;

int main (){
    ios::sync_with_stdio(false);
    int tc;
    cin >> tc;
    for(int tc0 = 1; tc0 <=tc; tc0++){
        int m, M[35], n, t;
        cin >> m;
        for(int i = 0; i < m ; i++){
            cin >> M[i];
        }
        cin >> n >> t;
        int win=0, loss= 0;
        vector<bool> C(m);
        fill(C.begin(), C.begin()+n, true);
        do{
            int sum = 0;
            for(int i = 0; i < m ; i++) if(C[i]) sum += M[i];
            sum == t ? win ++ : loss++;
        } while(prev_permutation(C.begin(), C.end()));

        cout << "Game " << tc0 << " -- "<< win << " : " << loss << endl;
    }
    return 0;
}