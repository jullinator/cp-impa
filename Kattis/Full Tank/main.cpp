/*
* Dijkstra [dist, fuel, v2]  +  DP (?) [How much fuel to fill up with]
* IF fuel < 0: continue 
* First one to reach goal is the shortest distance, whilst having fuel enough
*/
#include <cstdio>
#include <cstring>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#define INF 2000000000

using namespace std;


typedef pair<int, int> ii;
typedef tuple <int, int, int> iii;
typedef vector<int> vi;
typedef vector<ii> vii;


int cities, roads;


int main (){
    scanf("%d %d\n", &cities, &roads);

    vi prices (cities);
    vector<vii> adj(cities);

    for(int i = 0; i < cities; i++){
        scanf("%d", &prices[i]);
    }
    for(int i = 0; i < roads ; i++){
        int v1, v2, w;
        scanf("%d %d %d\n", &v1, &v2, &w);
        adj[v1].push_back(ii(v2, w));
        adj[v2].push_back(ii(v1, w));
    }

    int q;
    scanf("%d\n", &q);
    for(int i = 0; i < q; i++){
        int capacity, s, t;
        scanf("%d %d %d\n", &capacity, &s, &t);
        priority_queue <iii, vector<iii>, greater<iii>> pq;
        vector<bool> visited (cities);
        pq.push(iii(0, 0, s));    // Dist is price
        vector<ii> dist(cities, ii(INF, -1));
        dist[s] = ii(0,0);
        while(!pq.empty()){
            iii p1 = pq.top(); pq.pop();
            int dist1 = get<0>(p1), fuel1 = get<1>(p1), v1 = get<2>(p1);
            //printf("%d", v1);
            if(v1 == t) break;
            //if(dist1 > dist[v1].first && fuel1 <= dist[v1].second ) continue;
            for(ii edge: adj[v1]){
                int v2 = edge.first, w2 = edge.second;
                int max_fill = capacity - fuel1;
                for(int fill = max_fill; fill+fuel1 >= w2 ; fill--){
                    int fuel2 = fill + fuel1 - w2;
                    int dist2 = fill * prices[v1] + dist1;
                    pq.push(iii(dist2, fuel2, v2));
                    /*if(dist2 < dist[v2].first){
                        if(dist2 < dist[v2].first) dist[v2] = ii(dist2, fuel2);
                        
                    }*/
                }
            }
        }
        if(dist[t].first == INF){
            printf("impossible\n");
        } else{
            printf("%d\n", dist[t].first);
        }

    }
    


    return 0;
}