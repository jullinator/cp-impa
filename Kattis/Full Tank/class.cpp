/*
* Dijkstra [dist, fuel, v2]  +  DP (?) [How much fuel to fill up with]
* IF fuel < 0: continue 
* First one to reach goal is the shortest distance, whilst having fuel enough
*/
#include <cstdio>
#include <cstring>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#define INF 2000000000

using namespace std;

class Edge {
    public:
        int v, w;
        Edge(int _v, int _w){
            v = _v;
            w = _w;
        }
};

class Node {
    public:
        int dist, fuel, v;
        Node (int _dist, int _fuel, int _v){
            dist = _dist;
            fuel = _fuel;
            v = _v;
        }
        bool operator () (Node node1, Node node2){
            return node1.dist > node2.dist;
        }

};

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int cities, roads;


int main (){
    scanf("%d %d\n", &cities, &roads);

    vi prices (cities);
    vector<vector<Edge>> adj(cities);

    for(int i = 0; i < cities; i++){
        scanf("%d", &prices[i]);
    }
    for(int i = 0; i < roads ; i++){
        int v1, v2, w;
        scanf("%d %d %d\n", &v1, &v2, &w);
        adj[v1].push_back(Edge(v2, w));
        adj[v2].push_back(Edge(v1, w));
    }
    int q;
    scanf("%d\n", &q);
    for(int i = 0; i < q; i++){
        int capacity, s, t;
        scanf("%d %d %d\n", &capacity, &s, &t);
        priority_queue <Node, vector<Node>, Node> pq;
        pq.push(Node(0, 0, s));    // Dist is price
        vector<Node> dist(cities, Node(INF, -1, -1));
        dist[s] = Node(0,0,s) ;
        while(!pq.empty()){
            Node node1 = pq.top(); pq.pop();
            if(node1.v == t){
                break;
            }
            if(node1.dist > dist[node1.v].dist && node1.fuel <= dist[node1.v].fuel ) continue;
            for(Edge edge: adj[node1.v]){
                int max_fill = capacity - node1.fuel;
                for(int fill = max_fill; fill+node1.fuel >= edge.w ; fill--){
                    int fuel = fill+node1.fuel - edge.w;
                    Node node2 = Node(node1.dist + fill*prices[node1.v], fill+node1.fuel, edge.v);
                    if(node2.dist < dist[node2.v].dist || node2.fuel > dist[node2.v].fuel){
                        if(node2.dist < dist[node2.v].dist) dist[node2.v] = node2;
                        pq.push(node2);
                    }
                }
            }
        }
        if(dist[t].dist == INF){
            printf("impossible\n");
        } else{
            printf("%d\n", dist[t].dist);
        }

    }
    


    return 0;
}