#include <bits/stdc++.h>

using namespace std;

vector<int> pred;
vector<vector<int>> adj;



vector<pair<int,int>> get_bfs(string graph){
        int current_node = 0;
        int node_count = 0;
        vector<vector<int>> adj(graph.length()+1, vector<int>()); // Could be length + 1
        vector<int> pred(graph.length(), -1);
        for(char c: graph){
            cout << c;
            if(c == '0'){
                
                pred[node_count+1] = current_node;
                adj[current_node].push_back(node_count+1);
                current_node = ++node_count;
            } else {
                current_node = pred[current_node];
            }
        }
        cout << endl;
        vector<pair<int,int>> res(node_count+1);
        vector<int> dist(node_count+1, 1 << 30); // NODE_COUNT lol, note current_node
        queue<int> q;
        res[0] = make_pair(0, adj[0].size());
        q.push(0);
        dist[0] = 0;
        /*
        for(int i = 0; i < adj.size(); i++){
            for(int v: adj[i]){
                cout << v << " ";
            }
            cout << endl;
        } */
        while(!q.empty()){
            int v = q.front(); q.pop();
            //cout << v;
            //cout << adj[v].size();
            for(int v2: adj[v]){
                //cout << v2 << endl;
                if(dist[v]+1 < dist[v2]){
                    q.push(v2);
                    dist[v2] = dist[v] + 1;
                    //int size = (int) adj[v2].size();
                    res[v2] = make_pair(dist[v2], adj[v2].size());
                }
            }
            
        }
        
        sort(res.begin(), res.end());
        
        return res;
}

int main (){
    int T;
    cin >> T;
    for(int tc = 0; tc < T; tc++){
        string in;
        cin >> in;
        
        vector<pair<int,int>> res1 = get_bfs(in);
        
        cin >> in;
        vector<pair<int,int>> res2 = get_bfs(in);
        
        bool possible = true;
        if (res1.size() != res2.size()) possible = false;
        
        if(possible){
            for(int i = 0; i < res1.size(); i++){
                // res1[i].first != res2[i].first || res1[i].second != res2[i].second
                if(res1[i] != res2[i]){
                    possible = false;
                    break;
                }
            }
        }
        if (possible) {
            cout << "same" << endl;
        } else {
            cout << "different" << endl;
        }
    }

    return 0;
}