#include <bits/stdc++.h>
using namespace std;

int main (){
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    vector<int> a, b;
    int m, n;
    while (cin >> n >> m){
        if(!m && !n) break;
        a.clear(); b.clear();
        a.resize(n); b.resize(m);
        int cd;
        for(int i = 0; i < n; i++){
            cin >> cd;
            a[i] = cd;
        } 
        for(int i = 0; i < n; i++){
            cin >> cd;
            b[i] = cd;
            //b.push_back(cd);
        }
        int count = 0;
        int i = 0;
        for(int cd: a){
            while (i < b.size() && b[i] <= cd){
                if (b[i] == cd){
                    count++;
                }
                i++;
            }
        }
        cout << count << endl;
    }

    return 0;
}