
hash = hash * 101  +  *s++;

#include <bits/stdc++.h>
using namespace std;

int get_hash(string& word) {
    uint16_t hash = 0;
    for (char& c: word) {
        hash = hash*101 + c;
    }
    return hash;
}

int main (){
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    string dict [1<<16 + 1];
    string line;
    getline(cin, line);
    while(line.length() > 0){
        bool first = true;
        string trans, key;
        for(char c: line){
            if(c == ' '){
                first = false;
                continue;
            }
            if(first) trans += c;
            else      key += c;
        }
        dict[key] = trans;
        //cout << trans << " " << key << endl;
        getline(cin, line);
    }
    string key;
    while(cin >> key){
        string res = dict[key];
        if (res.length() > 0){
            cout << res << endl;
        } else {
            cout << "eh" << endl;
        }
    }

    return 0;
}