#include <bits/stdc++.h>
using namespace std;

int main (){
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    unordered_map<string, string> dict;
    string line;
    getline(cin, line);
    while(line.length() > 0){
        bool first = true;
        string trans, key;
        for(char c: line){
            if(c == ' '){
                first = false;
                continue;
            }
            if(first) trans += c;
            else      key += c;
        }
        dict[key] = trans;
        //cout << trans << " " << key << endl;
        getline(cin, line);
    }
    string key;
    while(cin >> key){
        string res = dict[key];
        if (res.length() > 0){
            cout << res << endl;
        } else {
            cout << "eh" << endl;
        }
    }

    return 0;
}