/*
*
*/
#include <cstdio>
#include <string.h>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <map>
#include <vector>
#define INF 2000000000

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef pair<float, int> fi;

int I = 0;
int mem [2005][2005]; // capacity, index 
int capacity;
int items;
int values [2005], weight[2005];

int solve(int c, int i){
    //if( c < 0 ) return -10000000; // This shouldn't happen
    if( i >= items) return 0;
    if(mem[c][i] != -1) return mem[c][i];
    //I++;
    if(c-weight[i] < 0){
        return mem[c][i] = solve(c, i+1);
    } else{
        int val = max(values[i]+solve(c-weight[i], i+1), solve(c, i+1));
        return mem[c][i] = val;
    }

}

int main (){
    float temp;
    while(scanf("%f %d\n", &temp, &items) != EOF){
        capacity = (int) temp;
        printf("%d", capacity);
        memset(values, -1, sizeof values);
        memset(weight, -1, sizeof weight);
        memset(mem, -1, sizeof mem);
        for(int i = 0; i < items ; i++){
            scanf("%d %d\n", &values[i], &weight[i]);
        }
        int val = solve(capacity, 0);

        // printf("%d\n", val);
        // print solution
        vector<int> ans;
        for(int i = 0; i < items-1 && capacity > 0; i++){
            int leave = mem[capacity][i+1]; // Don't take 
            int take = -1;
            if(mem[capacity-weight[i]][i+1] != -1 ){
                take = mem[capacity-weight[i]][i+1] + values[i];
            } 
            // Check if calc! -> default value = 0, 
            if(take >= leave){ // prioritize early index if similar
                ans.push_back(i);
                capacity -= weight[i];
            }
        }
        //printf("%d Times\n", I);
        printf("%d\n", ans.size());
        int i = 0;
        for(; i < ans.size()-1; i++){
            printf("%d ", ans[i]);
        }
        printf("%d\n", ans[i]);

    }    
    return 0;
}