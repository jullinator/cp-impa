/*
*
*/
#include <cstdio>
#include <string.h>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <map>
#include <vector>
#define INF 2000000000

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef pair<float, int> fi;

int I = 0;
map<fi, int> mem;
map<fi, bool> calc;
float capacity;
int items;
int values [2005], weight[2005];

int solve(float c, int i){
    //if( c < 0 ) return -10000000; // This shouldn't happen
    if( i >= items) return 0;
    if(calc[fi(c, i)]) return mem[fi(c, i)];
    //I++;
    if(c-weight[i] < 0){
        calc[fi(c,i)] = true;
        return mem[fi(c,i)] = solve(c, i+1);
    } else{
        int val = max(values[i]+solve(c-weight[i], i+1), solve(c, i+1));
        calc[fi(c,i)] = true;
        return mem[fi(c,i)] = val;
    }

}

int main (){

    while(scanf("%f %d\n", &capacity, &items) != EOF){
        memset(values, -1, sizeof values);
        memset(weight, -1, sizeof weight);
        mem.clear(); calc.clear();
        for(int i = 0; i < items ; i++){
            scanf("%d %d\n", &values[i], &weight[i]);
        }
        int val = solve(capacity, 0);
        //printf("%d\n", val);
        // print solution
        vector<int> ans;
        for(int i = 0; i < items; i++){
            int leave = mem[fi(capacity,i+1)]; // Don't take 
            int take = -1;
            if(calc[fi(capacity-weight[i], i+1)]){
                take = mem[fi(capacity-weight[i], i+1)] + values[i]; //WWWRRONG
            } 
            // Check if calc! -> default value = 0, 
            if(take >= leave){ // prioritize early index if similar
                ans.push_back(i);
                capacity -= weight[i];
            }
        }
        //printf("%d Times\n", I);
        printf("%d\n", ans.size());
        int i = 0;
        for(; i < ans.size()-1; i++){
            printf("%d ", ans[i]);
        }
        printf("%d\n", ans[i]);

    }    
    return 0;
}