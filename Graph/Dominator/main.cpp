#include <bits/stdc++.h>
#define MAX_V 100

int adj[MAX_V][MAX_V];
bool dominates[MAX_V][MAX_V];
bool visited[MAX_V];
int T, V;

void dfs(int node){
    visited[node] = true;
    for(int i = 0; i < V; i++){
        if(!visited[i] && adj[node][i])
            dfs(i);
    }
}

void print_break(){
    printf("+");
    for(int i = 0; i < V*2-1; i++){
        printf("-");
    }
    printf("+\n");
}

int main(){
    
    scanf("%d\n", &T);
    for(int t0 = 1; t0 < T+1; t0++){
        scanf("%d\n", &V);
        
        for(int i = 0; i < V; i++)
            for(int j = 0; j < V; j++)
                scanf("%d", &adj[i][j]);
        memset(dominates, false, sizeof(dominates));
        dominates[0][0] = true;
        for(int removed_node=1; removed_node < V; removed_node++){
            dominates[0][removed_node] = true;
            memset(visited, false, sizeof(visited));
            visited[removed_node] = true;
            dfs(0);
            for(int i=0; i < V; i++){
                if(!visited[i])
                    dominates[removed_node][i] = true;
            }
            dominates[removed_node][removed_node] = true;
        }
        memset(visited, false, sizeof(visited));
        dfs(0);
        printf("Case %d:\n", t0);
        print_break();
        for(int i=0; i < V; i++){
            printf("|");
            for(int j=0; j<V; j++){
                printf(dominates[i][j] && visited[j] ? "Y|" : "N|");
            }
            printf("\n");
            print_break();
        }
    }

    return 0;
}