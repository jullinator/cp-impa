#include <bits/stdc++.h>

using namespace std;

int numPoints = 10;

void mst(){
    // adding adj, pred, etc. broke it
    priority_queue<pair<double, int>, vector<pair<double,int>>, greater<pair<double,int>>> pq;

    unordered_set<int> unvisited;
    vector<vector<int>> adj(numPoints);
    vector<double> minCost(numPoints, INFINITY);
    vector<int> pred(numPoints, -1);

    for(int i = 0; i < numPoints; i++){
        unvisited.insert(i);
    }
    pq.push(make_pair(0.0, 0));
    cout << numPoints << endl;


    while(!pq.empty()){
        pair<double, int> p = pq.top();
        pq.pop();
        int v1 = p.second;
        cout << v1 << endl;
        if (unvisited.find(v1) == unvisited.end()) continue;
        unvisited.erase(v1);
        adj[v1].push_back(pred[v1]);
        adj[pred[v1]].push_back(v1);
        if(unvisited.size() == 0){
            break; // we're done
        }

        for(int v2 : unvisited){
            double weight = 4.0;
            cout << v2 << endl;
            if(weight < minCost[v2]){
                pq.push(make_pair(weight, v2));
                minCost[v2] = weight;
                pred[v2] = v1;
            }
        }
    }

    vector<int> oddDegrees;
    for(int v = 0; v<numPoints; v++){
        int degree = (int) adj[v].size();
        cout << degree << endl;
        if(degree % 2 == 1){
            oddDegrees.push_back(v);
        }
    }
    cout << "odds: ";
    for(int odd: oddDegrees) cout << odd << " ";


}

int main (){
    mst();
}