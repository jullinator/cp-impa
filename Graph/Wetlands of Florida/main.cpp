#include <bits/stdc++.h>
#define MAX_V 100
using namespace std;

int T, R, C;
vector<string> grid;
char sourceChar, targetChar;
int dr [] = {-1, -1, 0, 1, 1, 1, 0, -1};
int dc [] = {0, 1, 1, 1, 0, -1, -1, -1}; 

int component_size(int r, int c){
    int ans = 1;
    if (r<0 || r >= R || c<0 || c>= C || grid[r][c] != sourceChar)
        return 0;
    grid[r][c] = targetChar;
    for(int i = 0; i < 8; i++)
        ans += component_size(r + dr[i], c + dc[i]);
    return ans;
}

int main (){
    ios_base::sync_with_stdio(false);
    cin >> T;
    int base_val = 'X' + 'W';
    for(int t0 = 0; t0 < T; t0++){
        grid.clear();
        string line;
        cin >> ws;
        while(isalpha(cin.peek())){
            cin >> line >> ws;
            grid.push_back(line);
        }
        R = grid.size();
        C = grid[0].size();
        int r, c;
        while(isdigit(cin.peek())){
            cin >> r >> c >> ws;
            r--; c--;
            
            sourceChar = grid[r][c];
            targetChar = base_val - sourceChar;
            int ans = component_size(r, c);
            cout << ans << "\n";
        }
        if (t0 < T-1)
            cout << "\n";
    }
    return 0;
}
