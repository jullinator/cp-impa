// desc
#include <cstdio>
#include <math.h>
#include <algorithm>
#include <vector>

using namespace std;


class Soldier {
    public:
        Soldier *prev, *next;
        int id;

        Soldier (int _id){
            id = _id;
        }
};

int main (){

    Soldier s1(1);
    Soldier s2(2);
    Soldier s3(3);
    s1.next = &s2;
    s2.prev = &s1;
    s2.next = &s3;
    s3.prev = &s2;
    return 0;
}