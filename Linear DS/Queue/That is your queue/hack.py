from collections import deque, defaultdict


tc0 = 0
while True:
    tc0 += 1
    P, C = map(int, input().split())
    if P == 0 and C == 0:
        break
    dq = deque(range(1,P+1))
    prio = defaultdict(int)

    print("Case %d:" % tc0)

    def next_person():
        p = dq.popleft()
        if prio[p] == 1:
            print(p)
            prio[p] = 2
            dq.append(p)

        elif prio[p] == 2:
            prio[p] = 0
            next_person()
        else:
            print(p)
            dq.append(p)

    for c1 in range(C):
        inp = input()
        if inp == "N":
            next_person()

        else:
            p = int(inp.split()[1])
            prio[p] = 1 
            dq.appendleft(p)
        