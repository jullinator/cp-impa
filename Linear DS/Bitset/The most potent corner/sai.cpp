/*
 * Sai Cheemalapati
 * UVA 10264: The most potent corner
 *
 */


#include<algorithm>
#include<cmath>
#include<iostream>
#include<cstdio>
using namespace std;


int N;
int edges[50000];
int sum[50000];


int main() {
    ios::sync_with_stdio(false);
    while(true) {
        if(scanf("%d", &N) != 1) break;
        int num_edges = 1 << N;
        int max_sum = 0;
        for(int i = 0; i < num_edges; i++)
            scanf("%d", &edges[i]);
        for(int i = 0; i < num_edges; i++) {
            int edge_sum = 0;
            for(int j = 0; j < N; j++) {
                int b = i ^ (1 << j);
                edge_sum += edges[b];
            }
            sum[i] = edge_sum;
        }
        for(int i = 0; i < num_edges; i++) {
            for(int j = 0; j < N; j++) {
                int b = i ^ (1 << j);
                max_sum = max(max_sum, sum[i] + sum[b]);
            }
        }
        printf("%d\n", max_sum);
    }
}