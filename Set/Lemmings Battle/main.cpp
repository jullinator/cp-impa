/*
* Multiset - https://www.geeksforgeeks.org/multiset-in-cpp-stl/
*/
#include <iostream>
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <set>
#include <vector>
#define INF 2000000000

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef multiset<int, greater<int>> :: iterator ms_it;

int B, SG, SB;
multiset<int, greater<int>> sg, sb;

void print(){
    for(ms_it it = sg.begin(); it!= sg.end(); ++it){
        cout << *it << " ";
    }
    cout << endl;
}
void test(){
    sg.insert(20);
    sg.insert(20);
    sg.insert(50);
    sg.insert(10);
    sg.insert(60);
    print();
    ms_it itr = sg.find(20);
    if(itr != sg.end()) sg.erase(itr);
    print();
}


int main (){
    ios::sync_with_stdio(false);
    int tc;
    cin >> tc;

    int tc0 = 0;
    while(tc--){
        if(tc0 != 0){
            cout << endl;
        }
        tc0++;
        sg.clear(); sb.clear();
        cin >> B >> SG >> SB;
        for(int i = 0; i < SG; i++){
            int strength;
            cin >> strength;
            sg.insert(strength);
        }
        for(int i = 0; i < SB; i++){
            int strength;
            cin >> strength;
            sb.insert(strength);
        }

        string res = "green and blue died";

        while(true){
            int count = 0;
            vector<int> g, b;
            for(ms_it itr = sg.begin(); itr!= sg.end() && count != B; ++itr){
                int s = *itr;
                g.push_back(s);
                count ++;
            }
            count = 0;
            for(ms_it itr = sb.begin(); itr!= sb.end() && count != B; ++itr){
                int s = *itr;
                b.push_back(s);
                count ++;
            }
            int fighters = min(g.size(), b.size());
            for(int i = 0; i < fighters; i++){
                int g1 = g[i], b1 = b[i];
                if(g1 < b1){
                    g[i] = 0;
                    b[i] -= g1;
                } else if(g1 > b1){
                    g[i] -= b1;
                    b[i] = 0;
                } else {
                    g[i] = b[i] = 0;
                }
            }
            ms_it itr = sb.begin();
            for(int i = 0; i < fighters; i++) ++itr;
            sb.erase(sb.begin(), itr);
            itr = sg.begin();
            for(int i = 0; i < fighters; i++) ++itr;
            sg.erase(sg.begin(), itr);
            for(int s: g) if(s!=0) sg.insert(s);
            for(int s: b) if(s!=0) sb.insert(s);
            if (!sb.size() && !sg.size()) break;
            else if(!sb.size()){
                res = "green wins"; break;
            }
            else if(!sg.size()){
                res = "blue wins"; break; 
            }
        }
        cout << res;

    }
    return 0;
}