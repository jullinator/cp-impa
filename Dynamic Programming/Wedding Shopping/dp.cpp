// https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=2445
#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

int M, C, price[25][25];    // 

int memo [210][25];         // [Money states][Current Garment States]

int solve(int money, int g){
    if (money < 0) return -100000000;
    if (g == C) return M-money; // Total money spent, should be as high as possible
    if (memo[money][g] != -1) return memo[money][g];

    int ans = -1;

    for (int model = 1; model <= price[g][0]; model++){
        ans = max(ans, solve(money-price[g][model], g+1));
    }
    return memo[money][g] = ans;
}

int main (){

    int i, j, TC, score;
    scanf("%d\n", &TC);
    while(TC--){
        scanf("%d %d", &M, &C); // Available Money, Number of Garments to buy
        for (i = 0; i < C; i++){
            scanf("%d", &price[i][0]);  // Store number of items in index 0
            for (j = 1; j <= price[i][0]; j++) scanf("%d", &price[i][j]);
        }

        memset(memo, -1, sizeof memo);
        score = solve(M, 0);
        if(score < 0) printf("no solution\n");
        else          printf("%d\n", score);
    }

    return 0;
}