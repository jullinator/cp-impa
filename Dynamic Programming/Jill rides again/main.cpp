// Kadane's Algortihm
// Really complicated by the fact that you have to keep track of the range for optimal solution
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

vector<int> roads;

int main () {
    int TC;
    scanf("%d", &TC);
    while (TC--){
        roads.clear();
        int s;
        scanf("%d", &s);
        for (int i = 0; i < s-1; i++){
            int c;
            scanf("%d", &c);
            roads.push_back(c);
        }
        int sum = 0, ans = 0;
        int s1 = 0, s2 = 0, bs1 = 0, bs2 = 0;
        for (int i = 0; i < roads.size(); i++){
            int cost = roads[i];
            sum += cost;
            if (sum > ans){
                ans = sum;
                s2 = i;
            }
            if (sum < 0){
                bs1 = s1; bs2 = s2;
                sum = 0;
                s1 = i+1;
            }

        }
    }

    return 0;
}