w, s, c, k = map(int, input().split())

ans = False

if k == 0:
    ans = False
elif w+c > k:
    if s < k:
        ans = True
    elif w+c <= 2*k and s <= k:
        ans = True 

else:
    if w+c < k:
        ans = True
    elif s <= k:    # w+c == k
        ans = True 

if ans == True:
    print("YES")
else:
    print("NO")