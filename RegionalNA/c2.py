num, right = raw_input().split()
right = int(right)

while len(num) < 30:
    num += num[-right:]

num = float(num)

eps = 1e-10
a, b = 0, 1
while True:
    a = num*b
    if abs(a-round(a)) < eps :
        a = round(a)
        break
    else:
        b += 1

print "%d/%d" % (a, b)