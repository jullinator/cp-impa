n = int(input())


perms = [1]*(n+1)
for i in range(1,n+1):
    perms[i] = i*perms[i-1]

e = 0.0
for i in range(n+1):
    e += 1/perms[i]
print(e)