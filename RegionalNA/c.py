num, repeating = input().split()
repeating = int(repeating)
to_left = len(num) - repeating - len(num.split(".")[0]) - 1


def gcd(x, y):
    while y != 0:
        x, y = y, x % y 
    return x

x = float(num)
c1 = 10**(repeating+to_left)
x2 = x * c1
c2 = 10**(to_left)
x3 = c2*x 
a = int(x2 - int(x3))
b = int(c1-c2)
d = gcd(a, b)
a = int(a/d)
b = int(b/d)
print("{}/{}".format(a, b))