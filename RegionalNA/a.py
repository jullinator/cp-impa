from collections import deque

class Node(object):
    def __init__(self):
        self.children = []
        for i in xrange(26):
            self.children.append(None)
        self.count = 0

    



def insert(root, word):
    node = root 
    for c in word:
        idx = ord(c) - ord('a')
        if node.children[idx] == None:
            node.children[idx] = Node()
        node.count += 1
        node = node.children[idx]
    
        

n, q = map(int, raw_input().split())
pokes = [raw_input() for _ in xrange(n)]
for _ in xrange(q):
    k0, length = map(int, raw_input().split())
    root = Node() 
    for i in [int(x)-1 for x in raw_input().split()]:
        insert(root, pokes[i])
    ans = 0
    q = deque()
    q.append(root)
    visited = set()
    while q:
        node = q.popleft()
        if node != root and node.count == length:
            ans += 1
        for i in xrange(26):
            node2 = node.children[i]
            if node2 != None and node2 not in visited and node2.count >= length:
                visited.add(node2)
                q.append(node2)
    print ans

    
