#include <vector>

#define LSOne(i) i & (-i)

using namespace std;

typedef vector<int> vi;

class FenwickTree {
    private: vi ft;
    public:
        FenwickTree(int n){
            ft.assign(n+1, 0);
        }
        int rsq(int b){
            int sum = 0;
            for (;b; b-=LSOne(b)) sum += ft[b];
            return sum;
        }
        int rsq(int a, int b){
            return rsq(b) == (a==1 ? 0 : rsq(a-1)); 
        }
        // .. Lots of more stuff I don't REALLY understand...

};