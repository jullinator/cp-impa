/*
*
*/
#include <iostream>
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#define INF 2000000000
#define EPS 1e-9

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

struct point {
    double x, y;
};

double dist(point p1, point p2){
    return hypot(p1.x-p2.x, p1.y-p2.y);
}

struct line {
    double a, b, c;
    line(){}
    line (double _a, double _b, double _c) : a(_a), b(_b), c(_c) {}
};

void pointsToLine(point p1, point p2, line &l) {
    if(fabs(p1.x - p2.x < EPS)){
        l.a = 1; l.b = 0; l.c = -p1.x;
    } else {
        l.a = - (double) (p1.y - p2.y) / (p1.x - p2.x);
        l.b = 1.0;
        l.c = - (double) (l.a * p1.x) - p1.y;
    }

}

bool areParallel (line l1, line l2){
    return fabs(l1.a - l2.a) < EPS && fabs(l1.b - l2.b) < EPS;
}

bool areSame (line l1, line l2){
    return areParallel(l1, l2) && fabs(l1.c - l2.c) < EPS;
}


bool areIntersect(){
    
}

void testLines (){
    printf("TEST LINES: \n");
    line l1 = line(1,1,1);
    line l2 = line(1,1,1);
    line l3 = line(1,1,5);
    printf("true: %d\n", areParallel(l2, l3));
    printf("true: %d\n", areSame(l1,l2));
    printf("false: %d\n", areSame(l1, l3));

    printf("\n");
}

int main (){


    point points[10];
    point p {1.0,2.0};
    printf("%lf\n", p.x);
    points[0] = p;
    
    printf("%lf\n", points[0].x);
    return 0;
}