/*
*
*/
#include <iostream>
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#define INF 2000000000
#define EPS 1e-9

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

struct point {
    double x, y;
    point(double _x, double _y): x(_x), y(_y) {}
};

double dist(point p1, point p2){
    return hypot(p1.x-p2.x, p1.y-p2.y);
}

struct line {
    double a, b, c;
    line(){}
    line (double _a, double _b, double _c) : a(_a), b(_b), c(_c) {}
};

void pointsToLine(point p1, point p2, line &l) {
    if(fabs(p1.x - p2.x < EPS)){
        l.a = 1; l.b = 0; l.c = -p1.x;
    } else {
        l.a = - (double) (p1.y - p2.y) / (p1.x - p2.x);
        l.b = 1.0;
        l.c = - (double) (l.a * p1.x) - p1.y;
    }

}

bool areParallel (line l1, line l2){
    return fabs(l1.a - l2.a) < EPS && fabs(l1.b - l2.b) < EPS;
}

bool areSame (line l1, line l2){
    return areParallel(l1, l2) && fabs(l1.c - l2.c) < EPS;
}


double mX, mY;
int N;
vector<point> points;

int main (){
    while(scanf("%lf\n", &mX) != EOF){
        points.clear();

        scanf("%lf\n", &mY);
        points.push_back(point(mX, mY)); 
        scanf("%d\n", &N); 
        for(int i = 0; i < N+1; i++){
            double x, y;
            scanf("%lf\n", &x);
            scanf("%lf\n", &y);
            point p = point(x,y);
            points.push_back(p);
            line l = line();
            pointsToLine(points[0], p, l);
            printf("%lf %lf %lf\n", l.a, l.b, l.c);
        }
    }
    return 0;
}