// https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=1895

#include <cstdio>
#include <iostream>
#include <queue> 
#include <vector> 
#include <algorithm>
#include <functional>
using namespace std;

typedef vector<int> vi;
typedef long long ll;


int N;
priority_queue <int, vi, greater<int> > pq;

int main (){

    //scanf("%d\n", N);
    int i = 0;
    while(true){
        cin >> N;
        if(N == 0) break;
        pq.empty();
        while(N--){
            int n;
            //scanf("%d", &n);
            cin >> n;
            pq.push(n);
        }
        ll cost = 0;
        int x = pq.top(); pq.pop();
        while (!pq.empty()){
            int y = pq.top(); pq.pop();
            x += y;
            cost += x; 
        }
        //printf("%d\n", cost);
        cout << cost << endl;
        

    }

    return 0;
}