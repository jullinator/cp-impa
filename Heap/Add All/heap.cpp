// https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=1895

#include <cstdio>
#include <queue> 
#include <vector> 
#include <algorithm>
#include <functional>
using namespace std;

typedef vector<int> vi;


int N;
priority_queue <int, vi, greater<int> > pq;

int main (){
    pq.empty();
    pq.push(1);
    pq.push(2);
    pq.push(3);
    int cost = 0;
    int x = pq.top(); pq.pop();
    while (!pq.empty()){
        int y = pq.top(); pq.pop();
        x += y;
        cost += x; 
    }
    printf("%d\n", cost);
    
    return 0;
}