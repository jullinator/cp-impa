#include <cstdio>


using namespace std;

int T, A, B, C;

int main(){
    scanf("%d\n", &T);
    while(T--){
        scanf("%d %d %d\n", &A, &B, &C);
        bool sol = false;
        for(int x = -100; x <= 100; x++)
            for(int y = -100; y <= 100; y++)
                for(int z = -100; z <= 100; z++){
                    if(x != y && x != z && y != z && (x+y+z) == A && x*y*z == B && x*x+y*y+z*z == C ){
                        if(!sol) printf("%d %d %d\n", x, y, z);
                        sol = true;
                    }
                }
            
        
        if(!sol) printf("No solution.\n");
    }
    return 0;
}