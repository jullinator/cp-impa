
def catalan(n):
    res = 1
    for i in range(2, n+1):
        res*= ((i+n)/i)
    return round(res)

i = 0
while True:
    try:
        if i!= 0: input()
        n = int(input())
        if i != 0: print()
        i += 1
        print(catalan(n))
    except EOFError:
        break