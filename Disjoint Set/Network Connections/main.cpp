//  https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=734
// Trivial; just use disjoint sets
#include <cstdio>
#include <vector>
#include <string>
using namespace std;

typedef  vector<int>  vi;

class UnionFind {
    private: vi p, rank;
    public:
        UnionFind (int N){
            rank.assign(N, 0);
            p.assign(N, 0);
            for (int i=0; i<N ; i++)
                p[i] = i;
        }
        int find(int x){
            return x == p[x] ? x : p[x] = find(p[x]) ;
        }
        bool isSame(int x, int y){
            return find(x) == find(y);
        }

        void merge(int i, int j){
            if (!isSame(i, j)){
                int x = find(i), y = find(j);
                if (rank[x] < rank[y]) p[x] = y;
                else{
                    p[y] = x;
                    if(rank[x] == rank[y]) rank[x]++;
                }
            }
        }

        
};

int maint (){
    int T;
    scanf("%d\n", &T);
    int ORG = T;
    while(T--){
        if(T<ORG) printf("\n");
        int N;
        UnionFind set(N);
        scanf("\n%d\n", &N);
        int success, failure;
        while(true){
            string type;
            int x, y;
            scanf("%s %d %d\n", &type, &x, &y);
            // if(type == null) break;  // Stupid question, really hard to break loop at an empty line, in any programming language tbh -> tldr; loop will never break
            if(type=="c"){
                set.merge(x,y);
            } else {
                if(set.isSame(x,y)){
                    success ++;
                } else {
                    failure ++;
                }
            }
        }
        printf("%d,%d\n");
    }
    return 0;
}