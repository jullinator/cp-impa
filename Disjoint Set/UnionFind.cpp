
#include <vector>
#include <iostream>
using namespace std;
typedef  vector<int>  vi;


class UnionFind {
    private: vi p, rank;
    public:
        UnionFind (int N){
            rank.assign(N, 0);
            p.assign(N, 0);
            for (int i=0; i<N ; i++)
                p[i] = i;
        }
        int find(int x){
            return x == p[x] ? x : p[x] = find(p[x]) ;
        }
        bool isSame(int x, int y){
            return find(x) == find(y);
        }

        void merge(int i, int j){
            if (!isSame(i, j)){
                int x = find(i), y = find(j);
                if (rank[x] < rank[y]) p[x] = y;
                else{
                    p[y] = x;
                    if(rank[x] == rank[y]) rank[x]++;
                }
            }
        }

        
};

int main(){
    UnionFind set (10);
    set.merge(1,2);
    cout << set.isSame(1,2) << endl; 
    cout << set.isSame(2,3) << endl;
    set.merge(1,3);
    cout << set.isSame(2,3) << endl;

    return 0;
}