/*
*
*/
#include <iostream>
#include <cstdio>
#include <tuple>
#include <math.h>
#include <algorithm>
#include <queue>
#include <functional>
#include <string>
#include <vector>
#define INF 2000000000

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int main (){
    ios::sync_with_stdio(false);
    int tc;
    cin >> tc;
    while(tc--){
        int n;
        cin >> n;
        vector<int> coins(n);
        for(int n1 = 0; n1 < n; n1++){
            cin >> coins[n1];
        }
        int count = 1; // First one is always possible
        vector<bool> possible(n, true);
        for(int i = 1; i < n; i++){
            int sum = 0;
            for(int j = i-1; j > -1; j--){
                if(possible[j]) sum += coins[j];
            }
            if (sum < coins[i]) count++;
            else possible[i] = false;  
        }
        cout << count << endl;
    }
    return 0;
}