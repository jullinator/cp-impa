// desc
#include <cstdio>
#include <math.h>
#include <algorithm>
#include <vector>

using namespace std;

int main (){
    int TC;
    scanf("%d\n", &TC);
    while(TC--){
        int x, y;
        scanf("%d %d\n", &x, &y);
        if(x < y) printf("<\n");
        else if(x > y) printf(">\n");
        else printf("=\n");
    }
    return 0;
}