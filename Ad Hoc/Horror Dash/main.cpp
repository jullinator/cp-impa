// desc
#include <cstdio>
#include <math.h>
#include <algorithm>
#include <vector>

using namespace std;

int main (){
    int T;
    scanf("%d\n", &T);
    for (int i = 1; i <= T; i++){
        int n;
        scanf("%d", &n);
        vector<int> C;
        while(n--){
            int c;
            scanf("%d", &c);
            C.push_back(c);
        }
        int fastest = -1;
        for(int c: C){
            fastest = max(fastest, c);
        }
        printf("Case %d: %d\n", i, fastest);
    }
    return 0;
}